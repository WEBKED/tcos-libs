#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:

from __future__ import division, unicode_literals
import logging
import os
import signal
import sys

if __name__ == '__main__':
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Sets utf-8 (instead of latin1) as default encoding for every IO
	reload(sys); sys.setdefaultencoding('utf-8')
	# Run in application's working directory
	os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/.'); sys.path.insert(0, os.path.realpath(os.getcwd()))

# Configures logging
try:
	import tcos_logging
	tcos_logging.init(logger=logging.getLogger(__name__), default_level='WARNING')
except ImportError:
	# Attention: does not redirect stderr
	logging.basicConfig(level=logging.WARNING, datefmt='%H:%M:%S', format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s')

# Configures exception hook
try:
	import tcos_exception_hook
	tcos_exception_hook.init()
except ImportError:
	pass


def _get_settings_model(device=None, application=None):
	# Requests for a settings model
	try:
		import tcos_settings
		if device is not None:
			settings_model = tcos_settings.get_settings_model(device='{}/*'.format(device))[0]
		elif application is not None:
			settings_model = tcos_settings.get_settings_model(application='{}/*'.format(application))[0]
	except (ImportError, IndexError):
		settings_model = {}

	return settings_model


log_path = '/var/log/sso_check.log'


def run_init():
	logging.root.addHandler(logging.FileHandler(log_path, 'a'))
	logging.root.handlers[-1].formatter = logging.root.handlers[-2].formatter
	logging.getLogger(__name__).info('Launching %s...', __file__)

	sso_settings_model = _get_settings_model(device='sso')
	autologin_settings_model = _get_settings_model(device='autologin')

	if not sso_settings_model:
		logging.getLogger(__name__).info('No sso device assigned')
	elif not autologin_settings_model:
		logging.getLogger(__name__).info('No autologin device assigned')
	else:
		logging.getLogger(__name__).error('Two incompatible devices are assigned: "autologin" and "sso". One of them has to be removed. SingleSignOn will not work!')
		import tcos_notifier
		tcos_notifier.notify(
			template='warning',
			size='large',
			en_message='''
				<h4>Single sign-on</h4>
				<main>
					<p>Single sign-on and autologin are activated. These two settings are not compatible. Single sign-on will not work.</p>

					<p>Please contact your administrator.</p>
				</main>
			''',
			de_message='''
				<h4>Single Sign-on</h4>
				<main>
					<p>Single Sign-On und Autologin sind aktiviert. Diese beiden Einstellungen sind nicht kompatibel. Single Sign-On wird nicht funktionieren.</p>

					<p>Bitte informieren Sie Ihren Administrator.</p>
				</main>
			''',
		)


def _main():
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	_main()
