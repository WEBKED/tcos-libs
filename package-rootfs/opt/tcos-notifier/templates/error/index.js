$(function () {
	// Reads browser's (user's) default language
	var language = (navigator.language || navigator.userLanguage).split('-')[0]

	// Selects needed (or default) language button
	var $language_button = $("#language ." + language + " > a")
	if ($language_button.length != 0) {
		$language_button.tab('show')
		// $language_button = $("#language > .en > a")
	}
})
