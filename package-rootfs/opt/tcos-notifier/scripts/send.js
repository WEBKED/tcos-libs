jQuery(function ($) {
	window.send = function (container) {
		try {
			function UploadingFileException() {}  // Custom exception: files 'to upload' are found

			var $container = $(container)
			var timeout = undefined

			// Get all inputs of the container
			var $inputs = $("input", $container)
				.add($("textarea", $container))
				.add($("select", $container))

			var $button = $inputs.filter('[type="submit"]')

			// Clear all error-messages
			$inputs.each(_clear_messages)

			// Get action URI
			var action = typeof $container.attr('action') == 'string'? $container.attr('action'): (typeof $container.attr('href') == 'string'? $container.attr('href'): '')
			action.search(/\?/) == -1? action += '?': 0

			// Create callback-function for successful answers
			function on_receive_json($json) {
				try {
					_enable_sending($container, $button)
					_clear_messages()

					// Reset timeout
					if (typeof timeout == 'number') {
						clearTimeout(timeout)
						timeout = undefined
					}

					if ($json) {
						if ($json['errors']) {
							var focused = 0
							$.each($json['errors'], function (key, value) {
								var $input = $inputs.filter("[name='" + key + "']")  // .not("[disabled]")
								$input.length? 0: $input = $button
								$input
									.addClass('error')
									.keydown(_clear_messages)
									.change(_clear_messages)
									.before(
										$('<div class="error-message">' + value + '</div>')
										)
									//.prev(".error-message")
									//    .remove()

								if (!focused) {
									focused = 1
									$input.focus()
								}
							})
						}

						if ($json['values']) {
							$.each($json['values'], function (key, value) {
								var $input = $inputs.filter("[name='" + key + "']").not("[disabled]")
								if ($input.filter('input[type="file"]') || $input.filter('select')) {
									$input.attr('defaultValue', value)
								} else {
									$input.val(value)
								}
							})
						}

						if ($json['reload']) {
							window.location.reload()
						}

						if ($json['resend']) {
							send(container)
						}

						if ($json['redirect']) {
							setTimeout(function () { // Fix for saving in history
								window.location.href = $json['redirect']
							}, 1)
						}

						if ($json['message']) {
							//alert($json['message'])
							$button
								.keydown(_clear_messages)
								.change(_clear_messages)
								.before(
									$('<div class="message">' + $json['message'] + '</div>')
									)
								//.prev(".error-message")
								//    .remove()
						}

						if ($json['alert']) {
							alert($json['alert'])
						}
					}

				} catch (e) {
					if (typeof console != 'undefined') {
						console.info(e)
					}
				}
			}

			// Get all values from inputs
			var data = {}
			$inputs.each(function () {
					var $this = $(this)
					if ($this.attr('type') == 'checkbox') {
						if (!$this.is(':disabled') && $this.is(':checked') && $this.attr('value')) {
							data[$this.attr('name')] = (data[$this.attr('name')]? data[$this.attr('name')] + "": "") + $this.attr('value')  // Delimit values with ASCII records separator
						}
					} else if ($this.attr('type') == 'radio') {
						if ($this.attr('checked')) {
							data[$this.attr('name')] = $this.val()
						}
					} else if ($this.attr('type') == 'submit') {
						if ($this.attr('name')) {
							data[$this.attr('name')] = $this.attr('value')
						}
					} else if ($this.attr('type') == 'file') {
						var value_to_upload = $this.attr('value')
						var value = $this.attr('defaultValue')

						if (value_to_upload) {
							var files_data = new FormData()
							$.each($this.get(0).files, function(key, value) {
								files_data.append(key, value)
							})

							_disable_sending($container, $button)
							$this.next('.spinner').css('display', 'inline-block')

							$.ajax({
								url: action + (action.indexOf('?') === -1? '?': '&') + 'method=upload&name=' + $this.attr('name'),
								type: 'POST',
								data: files_data,
								cache: false,
								dataType: 'jsonp',
								processData: false,
								contentType: false,
							}).always(function() {
								_enable_sending($container, $button)
								$this.next('.spinner').hide()
							}).fail(function(jqXHR, msg, error) {
								if (typeof console != 'undefined') {
									console.info(error)
								}
							}).done(on_receive_json)

							// Break other actions until uploads
							$this.attr('value', '')
							throw new UploadingFileException()

						} else if (typeof value != 'undefined') {
							data[$this.attr('name')] = value
						}
					} else {
						data[$this.attr('name')] = ($this.val() != null)? $this.val(): ''
					}
				})

			// Fix for mod_security: encode all values
			$.each(data, function (key, value) {
				data[key] = encodeURIComponent(value)
			})

			// Send synchronously
			if (action.indexOf('admin-ajax.php') === -1) {
				window.location = action + (action.indexOf('?') === -1? '?': '&') + $.param(data)

			// Send asynchronously
			} else {
				// Send to server
				$.getJSON(action + '&callback=?', data, on_receive_json)

				// Disable submit-button and re-enable after timeout (if we receive no answer)
				_disable_sending($container, $button)
				timeout = setTimeout(  // TODO: remove previous timeout before call the new
					function () {
						try {
							if ($button.prop('disabled')) {
								$inputs
									.filter('[type="submit"]')
									.addClass('error')
									.keydown(_clear_messages)
									.change(_clear_messages)
									.before(
										$('<div class="error-message">Server is unreachable</div>')
										)
								_enable_sending($container, $button)
							}

						} catch (e) {
							if (typeof console != 'undefined') {
								console.info(e)
							}
						}
					},
					10000  // ms
				)
			}

		} catch (e) {
			if (e instanceof UploadingFileException) {
				// Do nothing

			} else {
				if (typeof console != 'undefined') {
					console.info(e)
				}
			}
		}
	}

	function _disable_sending($container, $button) {
		$button.prop('disabled', true)
		if ($container.is('a')) {
			$container.css('pointer-events', 'none')
		}
		$button.next('.spinner').css('display', 'inline-block')
	}

	function _enable_sending($container, $button) {
		$button.prop('disabled', false)
		if ($container.is('a')) {
			$container.css('pointer-events', 'auto')
		}
		$button.next('.spinner').hide()
	}

	function _clear_messages() {
		var $this = $(this)

		$this.each(function () {
				var $this = $(this)

				if ($this.hasClass('error')) {
					$this
						.removeClass('error')
						.prev(".error-message")
							.remove()
				}

				$this.prev(".message").remove()
			})
	}
})
