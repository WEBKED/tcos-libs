# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann

from __future__ import division, unicode_literals
import logging
import os
import signal
import sys

if __name__ == '__main__':
	# Runs in application's working directory
	sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/..')
	os.chdir(sys.path[0])
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Configures logging
	logging.basicConfig(
		level=logging.WARN, datefmt='%H:%M:%S',
		format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s',
	)
logging.getLogger(__name__).setLevel(getattr(logging, os.environ.get('LOGGING_' + __name__.replace('.', '_').upper(), os.environ.get('LOGGING', 'WARNING'))))

__doc__ = """Proxy for transparent selection between PyQt4 and PyQt5"""

try:
	if os.environ.get('WITH_QT4', False):
		raise ImportError()
	from PyQt5 import QtCore, QtGui, QtWidgets, QtWebKit, QtWebKitWidgets, uic
except ImportError:
	from PyQt4 import QtCore, QtGui, QtWebKit, uic
	# Force PyQt4 be alike PyQt5
	QtWidgets = QtGui
	QtWebKitWidgets = QtWebKit
	QtCore.qInstallMessageHandler = QtCore.qInstallMsgHandler

sys.modules['PyQt.QtWebKitWidgets'] = QtWebKitWidgets  # For uic-loader

logging.getLogger(__name__).debug('Using PyQt: %s', QtCore.QT_VERSION_STR)
uic  # To ignore pyflake warning
