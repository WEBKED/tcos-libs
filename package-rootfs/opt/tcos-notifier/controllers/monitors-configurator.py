#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann (gehrmann.mail@gmail.com)

from __future__ import division, unicode_literals; del division, unicode_literals  # Imports features but removes them from documentation
import functools
import logging
import os
import signal
import subprocess
import sys

if __name__ == '__main__':
	# Sets utf-8 (instead of latin1) as default encoding for every IO
	reload(sys); sys.setdefaultencoding('utf-8')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
# Inserts application's working directory into sys.path
sys.path.insert(0, os.path.realpath((os.path.dirname(os.path.realpath(__file__)) or '.') + '/../.'))

# Configures logging
try:
	import tcos_logging
	tcos_logging.init(logger=logging.getLogger(__name__))
except ImportError:
	# Attention: does not redirect stderr
	logging.basicConfig(level=logging.WARNING, datefmt='%H:%M:%S', format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s')

# Configures exception hook
try:
	import tcos_exception_hook
	tcos_exception_hook.init()
except ImportError:
	pass

from PyQt import QtCore, QtWidgets, uic

from controllers.notifier import NotifierController
from models.abstract import ObservableAttrDict


class _State(ObservableAttrDict):
	pass


class MonitorsConfiguratorController(NotifierController):
	def __init__(self, template='monitors_configuration', size='medium', auto_reload_template=False, *args, **kwargs):
		super(MonitorsConfiguratorController, self).__init__(template=template, size=size, auto_reload_template=auto_reload_template, *args, **kwargs)

		self.auto_reload_template = auto_reload_template
		self._monitors_views = []
		self._show_monitors_tool_tips()

	"""Model's event handlers"""

	"""View's event handlers"""

	def _on_browser_action_hide_monitors_tool_tips_clicked(self, state_model, widget):
		self._hide_monitors_tool_tips()

	def _on_browser_action_show_monitors_tool_tips_clicked(self, state_model, widget):
		self._show_monitors_tool_tips()

	def _on_browser_action_toggle_monitors_tool_tips_clicked(self, state_model, widget):
		if self._monitors_views:
			self._hide_monitors_tool_tips()
		else:
			self._show_monitors_tool_tips()

	def _on_browser_action_configure_monitors_sequentially_clicked(self, state_model, widget):
		_configure_monitors_sequentially()

	"""Helpers"""

	def _hide_monitors_tool_tips(self):
		for view in self._monitors_views:
			view.close()
		self._monitors_views[:] = []

	def _show_monitors_tool_tips(self):
		self._hide_monitors_tool_tips()

		desktop = self._app.desktop()

		connectors = _get_connectors()

		offset, width, height = 30, 400, 120
		x_offset = 0  # 1320 - size
		for index in range(self._app.desktop().screenCount()):
			# screen = self._app.desktop().screen(index)
			screen_geometry = desktop.screenGeometry(index)

			# Gets connector name by screen geometry
			key = '{:d}x{:d}{:+d}{:+d}'.format(
				screen_geometry.width(),
				screen_geometry.height(),
				screen_geometry.x(),
				screen_geometry.y(),
			)
			connector_name = next(k for k, v in connectors.items() if v['geometry'] == key)

			state_model = _State(**self._state_model)
			state_model.index = index + 1
			# state_model.connection_name = screen.name()
			state_model.connection_name = connector_name
			state_model.width = screen_geometry.width()
			state_model.height = screen_geometry.height()

			view = uic.loadUi(os.path.join(sys.path[0], 'views/main.ui'), QtWidgets.QMainWindow(parent=self._view))
			self._monitors_views.append(view)

			view.setWindowFlags(view.windowFlags() | QtCore.Qt.X11BypassWindowManagerHint)
			view.setGeometry(screen_geometry.x() + offset + x_offset, screen_geometry.y() + offset, width, height)  # Set screen (if part of virtual screen)
			# if hasattr(view, 'windowHandle'):
			#     view.windowHandle() is not None and view.windowHandle().setScreen(screen)  # Set screen (if not a part of virtual screen)

			# Sets handler for clicks on links
			view.browser.page().setLinkDelegationPolicy(view.browser.page().DelegateAllLinks)  # [ DontDelegateLinks | DelegateExternalLinks | DelegateAllLinks ]
			view.browser.linkClicked.connect(functools.partial(self._on_browser_link_clicked, state_model, view.browser))
			view.browser.urlChanged.connect(functools.partial(self._on_browser_url_changed, state_model, view.browser))

			template_path = os.path.join((os.path.dirname(os.path.realpath(__file__)) or '.') + '/../templates', state_model.template, 'monitor_tool_tip.html#' + unicode(index))
			view.browser.load(QtCore.QUrl('file://{}'.format(template_path)))

			# Reloads if template was edited (for developing purposes)
			if self.auto_reload_template:
				self._auto_reload_template(template_path=template_path.split('#', 1)[0], widget=view.browser)

			view.show()

			# cat /sys/class/drm/card0-LVDS-1/edid | parse-edid


def run_init():
	"""Shows notifier window. Use it as a command line tool."""
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, description=__doc__)
	parser.add_argument('--title', type=unicode, default='OTC Monitors Configuration', help='Window title')
	parser.add_argument('--size', type=unicode, choices=NotifierController.size_to_width_height, help='Window size')
	parser.add_argument('--auto-reload-template', type=lambda x: bool(x.replace('no', '')), choices=['yes', 'no', True, False], help='Enables/disables template reloading if was edited')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	_configure_monitors_sequentially()

	# sys.exit(MonitorsConfiguratorController(**kwargs).loop())
	MonitorsConfiguratorController(**kwargs).loop()

	_restore_monitors_configuration()


def run_show():
	"""Shows notifier window. Use it as a command line tool."""
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, description=__doc__)
	parser.add_argument('--title', type=unicode, default='OTC Monitors Configuration', help='Window title')
	parser.add_argument('--size', type=unicode, default='small', choices=NotifierController.size_to_width_height, help='Window size')
	parser.add_argument('--auto-reload-template', type=lambda x: bool(x.replace('no', '')), choices=['yes', 'no', True, False], help='Enables/disables template reloading if was edited')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# sys.exit(MonitorsConfiguratorController(**kwargs).loop())
	MonitorsConfiguratorController(**kwargs).loop()


def _get_connectors():
	# Requests for info about connectors
	command = 'LANG=C xrandr | grep " connected "'
	result = subprocess.check_output(command, shell=True)

	# Parses info about connectors
	connectors = {k: dict(raw=v) for k, v in (x.split(None, 1) for x in result.splitlines())}
	logging.getLogger(__name__).debug('connectors=' + '%s', connectors)

	for k, v in connectors.items():
		v['primary'] = ' primary ' in v['raw']
		v['geometry'] = v['raw'].replace(' primary ', ' ').split(None, 2)[1]

	return connectors


def _configure_monitors_sequentially():
	connectors = _get_connectors()

	primary_connector = next((k for k, v in connectors.items() if v['primary']), sorted(connectors)[0])
	secondary_connectors = [x for x in connectors if x != primary_connector]
	logging.getLogger(__name__).debug('primary_connector=' + '%s', primary_connector)
	logging.getLogger(__name__).debug('secondary_connectors=' + '%s', secondary_connectors)

	# Composes xrandr-command
	command = 'LANG=C xrandr --output {primary_connector} --primary --auto'.format(**locals())
	for connector, previous_connector in zip(secondary_connectors, [primary_connector] + secondary_connectors):
		command += ' --output {connector} --right-of {previous_connector} --auto'.format(**locals())
	logging.getLogger(__name__).debug('command=' + '%s', command)

	# Sets displays sequentially
	result = subprocess.check_output(command, shell=True)


def run_configure_monitors_sequentially():
	"""Entry point to dump xrandr-output. Only for developing purposes."""
	_configure_monitors_sequentially()


def _restore_monitors_configuration():
	# Requests for info about connectors
	command = 'sudo tcos-xrandr --show-notifications=no'
	subprocess.check_output(command, shell=True)


def run_restore_monitors_configuration():
	"""Entry point to restore monitors configuration. Only for developing purposes."""
	_restore_monitors_configuration()


def main():
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, add_help=False)
	parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Raises verbosity level for script (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs.get('verbose', 0), 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
