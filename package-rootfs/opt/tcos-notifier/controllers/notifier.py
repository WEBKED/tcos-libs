#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"
# (c) gehrmann (gehrmann.mail@gmail.com)

from __future__ import division, unicode_literals; del division, unicode_literals  # Imports features but removes them from documentation
import functools
import logging
import os
import pipes
import signal
import subprocess
import sys
import threading
import textwrap
import urllib
import webbrowser

import jinja2

if __name__ == '__main__':
	# Sets utf-8 (instead of latin1) as default encoding for every IO
	reload(sys); sys.setdefaultencoding('utf-8')
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
# Inserts application's working directory into sys.path
sys.path.insert(0, os.path.realpath((os.path.dirname(os.path.realpath(__file__)) or '.') + '/../.'))

# Configures logging
try:
	import tcos_logging
	tcos_logging.init(logger=logging.getLogger(__name__))
except ImportError:
	# Attention: does not redirect stderr
	logging.basicConfig(level=logging.WARNING, datefmt='%H:%M:%S', format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s')

# Configures exception hook
try:
	import tcos_exception_hook
	tcos_exception_hook.init()
except ImportError:
	pass

__doc__ = """
Tool to show decorated html-messages.

HowTo
=====

Use it as a command line tool:
	> notifier.py -r show [args]


"""

from PyQt import QtCore, QtGui, QtWidgets, QtWebKit, uic

from helpers.caller import Caller
from models.abstract import ObservableAttrDict


class _State(ObservableAttrDict):
	pass


class NotifierController(object):
	templates_path = (os.path.dirname(os.path.realpath(__file__)) or '.') + '/../templates'
	images_path = (os.path.dirname(os.path.realpath(__file__)) or '.') + '/../images'

	size_to_width_height = dict(
		xsmall=(500, 160),
		small=(500, 220),
		medium=(600, 280),
		large=(700, 380),
		xlarge=(800, 480),
	)

	def __init__(
			self,
			template='error',
			size='small',
			title='OTC Notification',
			message='',
			en_message='',
			de_message='',
			with_close_on_escape=True,
			close_after=None,
			auto_reload_template=False,
			show=True,
	):
		self._with_close_on_escape = with_close_on_escape

		self._app = app = QtWidgets.QApplication(sys.argv)

		# Models
		self._state_model = state_model = _State()
		state_model.template = template
		state_model.title = title
		state_model.message = message
		state_model.en_message = en_message
		state_model.de_message = de_message

		# Views
		self.__view = self._view = view = uic.loadUi(os.path.join(sys.path[0], 'views/main.ui'), QtWidgets.QMainWindow(parent=None))

		# view.keyPressEvent = self.__on_key_pressed
		# view.browser.keyPressEvent = self.__on_key_pressed
		view.keyPressEvent = (lambda widget, previous_callback: (lambda event: (self.__on_key_pressed(event, widget, previous_callback))))(view, view.keyPressEvent)
		view.browser.keyPressEvent = (lambda widget, previous_callback: (lambda event: (self.__on_key_pressed(event, widget, previous_callback))))(view.browser, view.browser.keyPressEvent)

		# Sets handler for clicks on links
		view.browser.page().setLinkDelegationPolicy(view.browser.page().DelegateAllLinks)  # [ DontDelegateLinks | DelegateExternalLinks | DelegateAllLinks ]
		view.browser.linkClicked.connect(functools.partial(self._on_browser_link_clicked, state_model, view.browser))
		view.browser.urlChanged.connect(functools.partial(self._on_browser_url_changed, state_model, view.browser))

		if os.environ.get('WITH_INSPECTOR', False):
			# Shows webview-inspector
			view.browser.settings().setAttribute(QtWebKit.QWebSettings.DeveloperExtrasEnabled, True)

			inspector_window = uic.loadUi(os.path.join(sys.path[0], 'views/inspector.ui'), QtWidgets.QMainWindow(parent=view))
			inspector_window.inspector.setPage(view.browser.page())
			screen_geometry = app.desktop().screenGeometry()
			inspector_window.setGeometry(screen_geometry.x(), screen_geometry.height() // 3 * 2, screen_geometry.width(), screen_geometry.height() // 3)
			inspector_window.show()

		# Sets icon
		icon = QtGui.QIcon()
		for filename in [x for x in sorted(os.listdir(self.images_path)) if x.startswith('icon_')]:
			width, height = [int(x) for x in filename.split('icon_', 1)[1].split('.png', 1)[0].split('x')]
			icon.addFile(os.path.join(self.images_path, filename), QtCore.QSize(width, height))
		view.setWindowIcon(icon)

		# Customizes window
		view.setWindowTitle(title)
		if 'balloon' in template:
			smallest_size = size = 400, 100
		else:
			smallest_size, size = sorted(self.size_to_width_height.values())[0], self.size_to_width_height[size]
		# Sets window size
		view.setMinimumSize(*smallest_size)  # Sets the smallest size
		# view.setMaximumSize(*size)
		view.resize(*size)
		if 'balloon' in template:
			self._with_close_on_escape = False

			# Customizes window style
			view.setWindowFlags(
				QtCore.Qt.Dialog |
				QtCore.Qt.Popup |
				QtCore.Qt.FramelessWindowHint |  # No window frame
				QtCore.Qt.WindowStaysOnTopHint |
				0
			)

			bar_height = app.desktop().availableGeometry().y()
			x_offset, y_offset = 10, 10
			# Calculates position between already opened balloons
			command = '(xwininfo -root -tree | grep "{}") || true'.format(title)  # Attention: view.windowTitle() can be here outdated
			result = subprocess.check_output(command, shell=True)
			disturbers = [
				dict(x=int(dx), y=int(dy), width=int(w), height=int(h))
				for x in result.splitlines()
				for w, h, x, y, dx, dy in [x.replace('x', ' ').replace('+', ' ').replace('-', ' -').rsplit(None, 6)[1:]]
			]
			disturber_bottom = bar_height
			for disturber in sorted(disturbers, key=lambda x: (x['y'])):
				if disturber['y'] > disturber_bottom + view.minimumSize().height():
					break
				disturber_bottom = disturber['y'] + disturber['height']
			# Sets window geometry to the top right
			view.setGeometry(
				app.desktop().geometry().x() + app.desktop().geometry().width() - view.minimumSize().width() - x_offset,
				app.desktop().geometry().y() + disturber_bottom + y_offset,
				view.minimumSize().width(),
				view.minimumSize().height(),
			)
		else:
			# Customizes window style
			view.setWindowFlags(
				QtCore.Qt.Window |
				# QtCore.Qt.FramelessWindowHint |  # No window frame
				QtCore.Qt.CustomizeWindowHint |  # No window decoration
				QtCore.Qt.WindowTitleHint |  # Show window title (only with menu, but can not close with Alt+F4)
				# QtCore.Qt.WindowMinimizeButtonHint |  # Window frame with minimize-button and menu
				0
			)
			# Moves window to the center of screen
			view.move(QtWidgets.QApplication.desktop().screenGeometry(QtWidgets.QApplication.desktop().primaryScreen()).center() - view.rect().center())

		template_path = os.path.join(self.templates_path, template, 'index.html')
		view.browser.load(QtCore.QUrl('file://{}'.format(template_path)))

		# Reloads if template was edited (for developing purposes)
		if auto_reload_template:
			self._auto_reload_template(template_path=template_path, widget=view.browser)

		if show:
			view.show()
			if 'balloon' in template:
				view.move(view.geometry().x(), view.geometry().y())  # Fix for WindowMaker: moves only existing window
		else:
			# Fix for avoiding launching application loop
			self.loop = lambda: 0

		if 'balloon' in template and close_after is None:
			close_after = 5.  # Default timeout for balloon template
		# Auto-closes window when timeout reached
		if close_after is not None and close_after:
			def close():
				view.close()
				if view.parent() is None:
					app.exit()

			# Checks if window has focus
			def focus_changed(from_widget, to_widget):
				if from_widget is None and to_widget is not None:
					Caller.call_never(close)
				if from_widget is not None and to_widget is None:
					Caller.call_once_after(float(close_after), close)
			app.focusChanged.connect(focus_changed)

			Caller.call_once_after(float(close_after), close)

		# Listens (in a separate thread) to data from piped stdin
		self._start_listening_to_stdin()

		# Observes models
		state_model.changed.bind(self._on_model_updated, invoke_in_main_thread=True)

		# Fills by models
		self._on_model_updated(state_model)

	"""Model's event handlers"""

	def _on_model_updated(self, model=None, previous=(None, ), current=(None, )):
		logging.getLogger(__name__).debug('%s %s %s %s', self.__class__.__name__, model.__class__.__name__, previous and str(previous)[:80], current and str(current)[:80])
		state_model = self._state_model

		if model is state_model:
			if current[0] is not None:
				# Updates browser by model
				self._update_browser_content()

	"""View's event handlers"""

	def __on_key_pressed(self, event, widget, previous_callback):
		if self._with_close_on_escape and event.key() == QtCore.Qt.Key_Escape:
			self.__view.close()
		previous_callback(event)

	def _on_browser_link_clicked(self, state_model, widget, url):
		url = unicode(url.toString())
		self._process_url(state_model, widget, url)

	def _on_browser_url_changed(self, state_model, widget, url):
		url = unicode(url.toString())
		self._process_url(state_model, widget, url)

	"""Helpers"""

	def loop(self):
		return self._app.exec_()

	def _auto_reload_template(self, template_path, widget):
		def update_if_template_was_changed(__state=dict(previous_mtime=None, was_visible=False)):
			current_mtime = os.path.getmtime(template_path)
			if __state['previous_mtime'] is not None and __state['previous_mtime'] < current_mtime:
				# widget.page().settings().clearMemoryCaches()
				widget.reload()
			__state['previous_mtime'] = current_mtime

			if not __state['was_visible'] and widget.isVisible():
				__state['was_visible'] = True
			elif __state['was_visible'] and not widget.isVisible():
				return

			Caller.call_once_after(1., update_if_template_was_changed)
		update_if_template_was_changed()

	def _start_listening_to_stdin(self):
		"""Launches listener in a separate thread"""
		listen_to_stdin_thread = threading.Thread(target=self._listen_to_stdin, kwargs=dict(self=self))
		listen_to_stdin_thread.daemon = True
		listen_to_stdin_thread.start()

	@staticmethod
	def _listen_to_stdin(self=None):
		"""Can be launched without object (for developing purposes)"""
		if not sys.stdin.isatty():  # If no interactive console attached
			with sys.stdin:
				for data in iter(sys.stdin.readline, ''):
					logging.getLogger(__name__).info('From stdin: %s', data.rstrip())

					# Parses (serialized) data from stdin
					import ast
					try:
						model = ast.literal_eval(data)
					except (SyntaxError, ValueError) as e:
						logging.getLogger(__name__).error('Wrong format (%s): %s', e, data.rstrip())
						# raise
					else:
						logging.getLogger(__name__).info('From stdin (parsed): %s', model)

						if self is not None:
							self._state_model[:] = model

	def _update_browser_content(self, state_model=None, widget=None, url=None):
		"""Fills browser content (by template and state_model)"""
		view = self.__view

		# Gets current state model
		if state_model is None:
			state_model = self._state_model

		# Gets current widget
		if widget is None:
			widget = view.browser

		# Gets current URI (if not set)
		if url is None:
			url = unicode(widget.url().toString())

		# Reads template from file
		path = url.split('file://', 1)[1].split('#', 1)[0]
		with open(path) as src:
			template = src.read()

		# Fills template
		# html = template.format(**state_model)
		html = jinja2.Environment(loader=jinja2.FileSystemLoader(searchpath=self.templates_path)).from_string(template).render(**state_model)

		widget.blockSignals(True)  # Ignores browser.urlChanged event
		widget.setContent(str(html), baseUrl=QtCore.QUrl(url))
		widget.blockSignals(False)  # Allows browser.urlChanged event

	def _decode_url_argument(self, value):
		if QtCore.QT_VERSION_STR[:2] == '4.':
			value = unicode(urllib.unquote(str(value)))
		else:
			value = unicode(urllib.unquote(urllib.unquote(str(value))))
		return value

	def _process_url(self, state_model, widget, url):
		"""Performs specified action (if ?action=...) or opens link in external browser"""
		view = self.__view

		if url.startswith('file://'):
			if '?' in url:
				kwargs = {k: self._decode_url_argument(v) for k, v in [x.split('=', 1) for x in url.split('?', 1)[1].split('&') if x]}
				action = kwargs.pop('action', '')
				# Closes window
				if action == 'close':
					view.close()
					if view.parent() is None:
						self._app.exit()
				# Closes window and prints out received data
				elif action == 'submit':
					view.close()
					print kwargs
				else:
					callback_name = '_on_browser_action_{}_clicked'.format(action)
					getattr(self, callback_name)(state_model, widget, **kwargs)
			else:
				self._update_browser_content(state_model, widget, url)

		else:
			# Opens link in browser
			webbrowser.open(url)


# def notify(**kwargs):
#     """Shows notifier window. Use it as a function from your python script."""
#     return NotifierController(**kwargs).loop()


def _wrap_for_x_user(function):
	"""Wraps command in order to run it from X-user"""
	if os.environ.get('USER', '') == 'root':
		def wrap():
			command = textwrap.dedent('''
				ck-list-sessions |  # X-sessions
					awk -F"'" '/unix-user/{{print $2}}' |  # Ids of X-users
					xargs getent passwd | awk -F: '{{print $1}}' |  # Usernames of X-users
					xargs -I{{user}} sudo /bin/su {{user}} -c {command}  # Runs command from username
			'''.format(
				command=pipes.quote('''
					DISPLAY=:0 {};
				'''.format(' '.join(pipes.quote(x) for x in sys.argv))
				)
			))
			logging.getLogger(__name__).debug('Command: %s', command)
			subprocess.Popen(command, shell=True, stderr=sys.stderr)  # Runs & forgets
		return wrap
	else:
		return function


@_wrap_for_x_user
def run_show():
	"""Shows notifier window. Use it as a command line tool."""
	templates = os.listdir(NotifierController.templates_path)

	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, description=__doc__)
	parser.add_argument('-t', '--template', type=unicode, required=True, choices=templates, help='Message template (from templates/)')
	parser.add_argument('--size', type=unicode, choices=NotifierController.size_to_width_height, help='Window size')
	parser.add_argument('--title', type=unicode, help='Window title')
	parser.add_argument('--message', type=unicode, help='Message body in HTML')
	parser.add_argument('--en-message', type=unicode, help='Message body (EN) in HTML')
	parser.add_argument('--de-message', type=unicode, help='Message body (DE) in HTML')
	parser.add_argument('--auto-reload-template', type=lambda x: bool(x.replace('no', '')), choices=['yes', 'no', True, False], help='Reloads template if was edited')
	parser.add_argument('--with-close-on-escape', type=lambda x: bool(x.replace('no', '')), choices=['yes', 'no', True, False], help='Enables/disables window close on ESC button')
	parser.add_argument('--close-after', type=float, help='Auto close window after timeout (in seconds)')
	parser.add_argument('--show', type=lambda x: bool(x.replace('no', '')), choices=['yes', 'no', True, False], help='Enables/disables showing (only for pre-loading notifier)')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	sys.exit(NotifierController(**kwargs).loop())


def run_show_exception_example():
	"""Shows notifier window filled with example text. Only for developing purposes."""
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, description=__doc__)
	parser.add_argument('-t', '--template', type=unicode, default='exception', help='Message template (from templates/)')
	parser.add_argument('--size', type=unicode, default='medium', choices=NotifierController.size_to_width_height, help='Window size')
	parser.add_argument('--title', type=unicode, default='Exception', help='Window title')
	parser.add_argument('--en-message', type=unicode, default=20 * 'Could not add Citrix store (tried twice). Look into <a href="http://wiki.openthinclient.org/en/">Documentation</a>.', help='Message body (EN)')
	parser.add_argument('--de-message', type=unicode, default=20 * 'Kann nicht den Citrix-Store einfügen. Schau mal in unsere <a href="http://wiki.openthinclient.org/de/">Dokumentation</a>.', help='Message body (DE)')
	parser.add_argument('--auto-reload-template', type=lambda x: bool(x.replace('no', '')), choices=['yes', 'no', True, False], default=True, help='Reloads template if was edited')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	sys.exit(NotifierController(**kwargs).loop())


def run_show_info_example():
	"""Shows notifier window filled with example text. Only for developing purposes."""
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, description=__doc__)
	parser.add_argument('-t', '--template', type=unicode, default='info', help='Message template (from templates/)')
	parser.add_argument('--size', type=unicode, default='small', choices=NotifierController.size_to_width_height, help='Window size')
	parser.add_argument('--title', type=unicode, default='Info', help='Window title')
	parser.add_argument('--en-message', type=unicode, default=20 * 'Could not add Citrix store (tried twice). Look into <a href="http://wiki.openthinclient.org/en/">Documentation</a>.', help='Message body (EN)')
	parser.add_argument('--de-message', type=unicode, default=20 * 'Kann nicht den Citrix-Store einfügen. Schau mal in unsere <a href="http://wiki.openthinclient.org/de/">Dokumentation</a>.', help='Message body (DE)')
	parser.add_argument('--auto-reload-template', type=lambda x: bool(x.replace('no', '')), choices=['yes', 'no', True, False], default=True, help='Reloads template if was edited')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	sys.exit(NotifierController(**kwargs).loop())


def run_show_warning_example():
	"""Shows notifier window filled with example text. Only for developing purposes."""
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, description=__doc__)
	parser.add_argument('-t', '--template', type=unicode, default='warning', help='Message template (from templates/)')
	parser.add_argument('--size', type=unicode, default='small', choices=NotifierController.size_to_width_height, help='Window size')
	parser.add_argument('--title', type=unicode, default='Warning', help='Window title')
	parser.add_argument('--en-message', type=unicode, default=20 * 'Could not add Citrix store (tried twice). Look into <a href="http://wiki.openthinclient.org/en/">Documentation</a>.', help='Message body (EN)')
	parser.add_argument('--de-message', type=unicode, default=20 * 'Kann nicht den Citrix-Store einfügen. Schau mal in unsere <a href="http://wiki.openthinclient.org/de/">Dokumentation</a>.', help='Message body (DE)')
	parser.add_argument('--auto-reload-template', type=lambda x: bool(x.replace('no', '')), choices=['yes', 'no', True, False], default=True, help='Reloads template if was edited')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	sys.exit(NotifierController(**kwargs).loop())


def run_show_error_example():
	"""Shows notifier window filled with example text. Only for developing purposes."""
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, description=__doc__)
	parser.add_argument('-t', '--template', type=unicode, default='error', help='Message template (from templates/)')
	parser.add_argument('--size', type=unicode, default='small', choices=NotifierController.size_to_width_height, help='Window size')
	parser.add_argument('--title', type=unicode, default='Error', help='Window title')
	parser.add_argument('--en-message', type=unicode, default=20 * 'Could not add Citrix store (tried twice). Look into <a href="http://wiki.openthinclient.org/en/">Documentation</a>.', help='Message body (EN)')
	parser.add_argument('--de-message', type=unicode, default=20 * 'Kann nicht den Citrix-Store einfügen. Schau mal in unsere <a href="http://wiki.openthinclient.org/de/">Dokumentation</a>.', help='Message body (DE)')
	parser.add_argument('--auto-reload-template', type=lambda x: bool(x.replace('no', '')), choices=['yes', 'no', True, False], default=True, help='Reloads template if was edited')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	sys.exit(NotifierController(**kwargs).loop())


def run_show_login_example():
	"""Shows notifier window filled with example text. Only for developing purposes."""
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, description=__doc__)
	parser.add_argument('-t', '--template', type=unicode, default='login', help='Message template (from templates/)')
	parser.add_argument('--size', type=unicode, default='small', choices=NotifierController.size_to_width_height, help='Window size')
	parser.add_argument('--title', type=unicode, default='Login dialog', help='Window title')
	parser.add_argument('--auto-reload-template', type=lambda x: bool(x.replace('no', '')), choices=['yes', 'no', True, False], default=True, help='Reloads template if was edited')
	parser.add_argument('--en-message', type=unicode, default='<h3>ICA login</h3>', help='Message body (EN)')
	parser.add_argument('--de-message', type=unicode, default='<h3>ICA Einloggen</h3>', help='Message body (DE)')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	sys.exit(NotifierController(**kwargs).loop())


def run_show_password_example():
	"""Shows notifier window filled with example text. Only for developing purposes."""
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, description=__doc__)
	parser.add_argument('-t', '--template', type=unicode, default='password', help='Message template (from templates/)')
	parser.add_argument('--size', type=unicode, default='small', choices=NotifierController.size_to_width_height, help='Window size')
	parser.add_argument('--title', type=unicode, default='Password dialog', help='Window title')
	parser.add_argument('--auto-reload-template', type=lambda x: bool(x.replace('no', '')), choices=['yes', 'no', True, False], default=True, help='Reloads template if was edited')
	parser.add_argument('--en-message', type=unicode, default='<h3>ICA password</h3>', help='Message body (EN)')
	parser.add_argument('--de-message', type=unicode, default='<h3>ICA Passwort</h3>', help='Message body (DE)')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	sys.exit(NotifierController(**kwargs).loop())


def run_show_server_domain_login_password_example():
	"""Shows notifier window filled with example text. Only for developing purposes."""
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, description=__doc__)
	parser.add_argument('-t', '--template', type=unicode, default='server_domain_login_password', help='Message template (from templates/)')
	parser.add_argument('--size', type=unicode, default='medium', choices=NotifierController.size_to_width_height, help='Window size')
	parser.add_argument('--title', type=unicode, default='Access dialog', help='Window title')
	parser.add_argument('--auto-reload-template', type=lambda x: bool(x.replace('no', '')), choices=['yes', 'no', True, False], default=True, help='Reloads template if was edited')
	parser.add_argument('--en-message', type=unicode, default='<h3>Credentials</h3>', help='Message body (EN)')
	parser.add_argument('--de-message', type=unicode, default='<h3>Zugriffsdateien</h3>', help='Message body (DE)')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	sys.exit(NotifierController(**kwargs).loop())


def run_show_confirmation_example():
	"""Shows notifier window filled with example text. Only for developing purposes."""
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, description=__doc__)
	parser.add_argument('-t', '--template', type=unicode, default='confirmation', help='Message template (from templates/)')
	parser.add_argument('--size', type=unicode, default='medium', choices=NotifierController.size_to_width_height, help='Window size')
	parser.add_argument('--title', type=unicode, default='Confirmation', help='Window title')
	parser.add_argument('--auto-reload-template', type=lambda x: bool(x.replace('no', '')), choices=['yes', 'no', True, False], default=True, help='Reloads template if was edited')
	parser.add_argument('--en-message', type=unicode, default='<h3>Credentials</h3>', help='Message body (EN)')
	parser.add_argument('--de-message', type=unicode, default='<h3>Zugriffsdateien</h3>', help='Message body (DE)')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	sys.exit(NotifierController(**kwargs).loop())


@_wrap_for_x_user
def run_show_balloon_example():
	"""Shows notifier balloon filled with example text. Only for developing purposes."""
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, description=__doc__)
	parser.add_argument('-t', '--template', type=unicode, default='balloon', help='Message template (from templates/)')
	parser.add_argument('--size', type=unicode, default='small', choices=NotifierController.size_to_width_height, help='Window size')
	# parser.add_argument('--title', type=unicode, default='Balloon title', help='Window title')
	# parser.add_argument('--message', type=unicode, default='<h5>Balloon message</h5>Some very long (and more) balloon content message', help='Message body (EN)')
	parser.add_argument('--en-message', type=unicode, default='<h5>Example balloon message (EN)</h5>Some balloon content message.', help='Message body (EN)')
	parser.add_argument('--de-message', type=unicode, default='<h5>Very long example balloon message (DE)</h5>Some very long balloon content message in order to force showing progress bar on the right side of balloon. Normally "close"-button has not to be overlapped.', help='Message body (DE)')
	parser.add_argument('--close-after', type=float, help='Auto close window after timeout (in seconds)')
	parser.add_argument('--auto-reload-template', type=lambda x: bool(x.replace('no', '')), choices=['yes', 'no', True, False], default=True, help='Reloads template if was edited')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	sys.exit(NotifierController(**kwargs).loop())


def run_show_progress_example():
	"""Shows notifier window filled with example text. Only for developing purposes.

	Example:
		> (sleep 2 && echo "{'en_message': 'TEST 1'}" && sleep 2 && echo "{'en_message': '<h2>TEST 2</h2>'}") | ./notifier.py -r show_progress_example -v
	"""
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, description=__doc__)
	parser.add_argument('-t', '--template', type=unicode, default='progress', help='Message template (from templates/)')
	parser.add_argument('--size', type=unicode, default='xsmall', choices=NotifierController.size_to_width_height, help='Window size')
	parser.add_argument('--title', type=unicode, default='Progress', help='Window title')
	parser.add_argument('--auto-reload-template', type=lambda x: bool(x.replace('no', '')), choices=['yes', 'no', True, False], default=True, help='Reloads template if was edited')
	parser.add_argument('--en-message', type=unicode, default='<h3>Processing...</h3>', help='Message body (EN)')
	parser.add_argument('--de-message', type=unicode, default='<h3>Im Prozeß...</h3>', help='Message body (DE)')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	sys.exit(NotifierController(**kwargs).loop())


def run_listen_to_stdin_example():
	"""Runs example code to read from stdin. Only for developing purposes.

	Example:
		> (sleep 2 && echo "{'en_message': 'TEST 1'}" && sleep 2 && echo "{'en_message': '<h2>TEST 2</h2>'}") | ./notifier.py -r listen_to_stdin_example -v
	"""
	NotifierController._listen_to_stdin()


def main():
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, add_help=False)
	parser.add_argument('-r', '--run-function', default='show', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Raises verbosity level for script (through arguments -v and -vv)
	logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs.get('verbose', 0), 2)])

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	main()
