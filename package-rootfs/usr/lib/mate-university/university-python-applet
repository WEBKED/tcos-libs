#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
#
# Copyright (C) 2013 Stefano Karapetsas
#
#  This file is part of MATE University.
#
#  MATE University is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  MATE University is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with MATE University.  If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#      Stefano Karapetsas <stefano@karapetsas.com>


# this code is based on example appet by Vincent Untz for GNOME Panel 3
# http://git.gnome.org/browse/gnome-panel/commit/?id=5ad4d9e

from __future__ import division, unicode_literals
import logging
import os
import signal
import sys

if __name__ == '__main__':
	# Working interruption by Ctrl-C
	signal.signal(signal.SIGINT, signal.default_int_handler)
	# Sets utf-8 (instead of latin1) as default encoding for every IO
	reload(sys); sys.setdefaultencoding('utf-8')
	# Run in application's working directory
	os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/.'); sys.path.insert(0, os.path.realpath(os.getcwd()))

# Configures logging
try:
	import tcos_logging
	tcos_logging.init(logger=logging.getLogger(__name__), default_level='WARNING')
except ImportError:
	# Attention: does not redirect stderr
	logging.basicConfig(level=logging.WARNING, datefmt='%H:%M:%S', format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s')

# Configures exception hook
try:
	import tcos_exception_hook
	tcos_exception_hook.init()
except ImportError:
	pass


def applet_factory(applet, iid, data):
	"""Called by mate-panel on applet creating"""
	if iid != 'UniversityPythonApplet':
		return False

	# You can use this path with gio/gsettings
	# settings_path = applet.get_preferences_path()

	import tcos_system
	ip = tcos_system.get_default_ip_address()

	from gi.repository import Gtk
	label = Gtk.Label('ip: ' + ip)
	applet.add(label)

	applet.show_all()

	return True


def _main():
	# Ensures we are using Gtk 2, not Gtk3. This will print a warning but everything should work.
	import gi
	gi.require_version('Gtk', '2.0')

	# Registers applet
	from gi.repository import MatePanelApplet
	MatePanelApplet.Applet.factory_main(
		'UniversityPythonAppletFactory',
		True,
		MatePanelApplet.Applet.__gtype__,
		applet_factory,
		None,
	)

if __name__ == '__main__':
	_main()
