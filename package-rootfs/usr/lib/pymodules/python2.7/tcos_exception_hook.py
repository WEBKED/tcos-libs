#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"

from __future__ import division, unicode_literals; del division, unicode_literals  # Imports features but removes them from documentation
import datetime
import logging
import os
import pipes
import sys
import traceback

__doc__ = """
Module to create error report and notify user if an exception occurs.

Also available for bash-scripts (see tcos_exception_hook.sh).

How to use it
=============

Insert:
	>>> import tcos_exception_hook
	>>> tcos_exception_hook.init()

or (to ignore exception if module is not available):
	>>> try:
	... 	import tcos_exception_hook
	... 	tcos_exception_hook.init()
	... except ImportError:
	... 	pass

into your script's section where imports of 3rd-party modules are.

Dependencies
============

* package 'python-qt4'
* symbolic link '/usr/bin/mate-open' => '/usr/bin/gnome-open'
* remote directory '/tcos/reports' (NFS)
"""

if __name__ == '__main__':
	# Sets utf-8 (instead of latin1) as default encoding for every IO
	reload(sys); sys.setdefaultencoding('utf-8')
	# Configures logging
	logging.basicConfig(level=logging.WARNING, datefmt='%H:%M:%S', format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s')

import tcos_notifier


def _collect_exception(etype, value, tb):
	import subprocess
	import tempfile
	import textwrap

	if etype not in (KeyboardInterrupt, ):
		import tcos_system

		# Creates reports only if /tcos/home was mounted earlier
		# reports_path = '/tcos/home/error-reports'
		reports_path = '/tcos/reports'
		if os.path.isdir(reports_path):
			message = tb if tb.__class__ is unicode else ''.join(traceback.format_exception(etype, value, tb))
			keep_reports_count = 20
			# report_path = os.path.join(reports_path, datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S.%f')[:-3])
			report_path = os.path.join('/tmp', datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S.%f')[:-3])
			home_path = os.environ.get('HOME', '')
			pid = os.getpid()

			# Saves traceback into temporary file (will be moved later into exception.log)
			(_, exception_path) = tempfile.mkstemp()
			with open(exception_path, 'w') as dst:
				dst.write(message)

			# Saves additional info: mounted partitions, content of /tcos/link (versions of installed packages, custom configuration, etc.)
			commands = textwrap.dedent("""
				sudo mkdir -p {_report_path}

				# Saves exception message
				sudo mv {_exception_path} {_report_path}/exception.log

				# Saves hostname and IP-address
				sudo hostname | sudo tee {_report_path}/hostname.log
				sudo ip address show | sudo tee {_report_path}/ip-address-show.log
				sudo ifconfig -a | sudo tee {_report_path}/ifconfig-a.log

				# Saves system information
				sudo uname -a | sudo tee {_report_path}/uname-a.log
				sudo cat /etc/issue | sudo tee {_report_path}/etc-issue.log
				sudo cat /proc/cpuinfo | sudo tee {_report_path}/proc-cpuinfo.log
				sudo cat /proc/meminfo | sudo tee {_report_path}/proc-meminfo.log
				sudo cat /proc/cmdline | sudo tee {_report_path}/proc-cmdline.log
				sudo free -m | sudo tee {_report_path}/free-m.log
				sudo dmidecode | sudo tee {_report_path}/dmidecode.log

				# Saves list of devices
				#sudo hwinfo | sudo tee {_report_path}/hwinfo.log
				#sudo lshw | sudo tee {_report_path}/lshw.log
				sudo lspci | sudo tee {_report_path}/lspci.log
				sudo lspci -nnk | sudo tee {_report_path}/lspci-nnk.log
				sudo lspci -v | sudo tee {_report_path}/lspci-v.log
				sudo lsusb | sudo tee {_report_path}/lsusb.log
				sudo lsusb -v | sudo tee {_report_path}/lsusb-v.log

				# Saves information about disk partitions
				sudo tail -n +3 /proc/partitions | grep -v loop | sudo tee {_report_path}/proc-partitions.log
				sudo tail -n +3 /proc/partitions | grep -v loop | awk '{{print "/dev/" $4}}' | xargs fdisk -l | sudo tee {_report_path}/proc-partitions-fdisk-l.log

				# Saves information about video connectors
				xrandr -q | sudo tee {_report_path}/xrandr-q.log

				# Saves dmesg
				sudo dmesg | sudo tee {_report_path}/dmesg.log

				# Saves Xorg.log
				sudo cp -f /var/log/Xorg*.log {_report_path}/

				# Saves .xsession-errors
				sudo cp -f {_home_path}/.xsession-errors {_report_path}/

				# Saves tree of /tcos/link
				sudo find /tcos/link -type f | sort -h | sudo tee {_report_path}/tcos_link_tree.log

				# Saves changelog-files of installed packages
				sudo find /tcos/link/sfs -type f -name "*.changelog" -print0 | sudo tar czvf {_report_path}/tcos_link_sfs_changelog.tgz --null -T -

				# Saves process data
				sudo mkdir -p {_report_path}
				sudo cp -f --parents --preserve=all $(sudo find /proc/{pid} -regex ".*/fd.*/[0-9]+" -o -iname mem -o -iname prev -o -iname current -o -iname exec -o -iname fscreate -o -iname keycreate -o -iname clear_refs -o -iname \*sock\* -o -iname \*map\* -o -iname \*cache\* -prune -o -type f -print | sudo xargs wc -c | awk '$1 > 0 && $1 < 65536 {{print $2}}') {_report_path}
				cd {_report_path}/proc && sudo tar cf ../proc.tar.gz . && sudo rm -rf {_report_path}/proc

				# Archives report into tgz
				cd {_report_path} && sudo find . -type f -print0 | sudo tar czvf {_report_path}.tgz --null -T -
				sudo rm -rf {_report_path}
				sudo mv {_report_path}.tgz {_reports_path}/

				# Keeps only latest N reports in a directory (prevents growing of files number)
				cd {_reports_path} && sudo ls . | head -n -{keep_reports_count} | sudo xargs rm -f

			""".format(
				_home_path=pipes.quote(home_path),
				_exception_path=pipes.quote(exception_path),
				_reports_path=pipes.quote(reports_path),
				_report_path=pipes.quote(report_path),
				**locals()
			))
			for command in [xx for x in commands.splitlines() for xx in [x.lstrip()] if xx and not xx.startswith('#')]:
				logging.getLogger(__name__).debug('> %s', command)
				from helpers.timer import Timer
				with Timer('running command "{}"'.format(command), limit=.5):
					tcos_system.run_command(command, stderr=subprocess.STDOUT)


def _show_notification(etype, value, tb):
	if etype not in (KeyboardInterrupt, ):
		message = tb if tb.__class__ is unicode else ''.join(traceback.format_exception(etype, value, tb))

		tcos_notifier.notify(
			template='exception',
			size='medium',
			title='Exception',
			en_message="""
				{message}
			""".format(message=message.replace('\n', '<br/>')),
			de_message="""
				{message}
			""".format(message=message.replace('\n', '<br/>')),
		)


def init(with_notification=True, with_collector=True):
	"""Initializes exception-hook for a module where was called.

	Arguments:
		with_notification (bool) -- notifies user about exception (default: True). Will be disabled if -n found or environment variable WITHOUT_EXCEPTION_NOTIFICATION was set.
		with_collector (bool) -- collects some information about exception and environment (default: True). Will be disabled if -n found or environment variable WITHOUT_EXCEPTION_COLLECTOR was set.

	"""
	# Looks for -d in arguments
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	# parser.add_argument('-v', action='count', help='Raises logging level')
	parser.add_argument('-n', '--no-exception-hook', action='store_true', help='Disables exception hook')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Disables everything if a special command line argument found
	if kwargs['no_exception_hook']:
		with_notification = False
		with_collector = False

	# Disables notifications if environment variable WITHOUT_EXCEPTION_NOTIFICATION was set
	if os.environ.get('WITHOUT_EXCEPTION_NOTIFICATION', '').lower().replace('false', '').replace('0', ''):
		with_notification = False

	# Disables collector if environment variable WITHOUT_EXCEPTION_COLLECTOR was set
	if os.environ.get('WITHOUT_EXCEPTION_COLLECTOR', '').lower().replace('false', '').replace('0', ''):
		with_collector = False

	# sys.displayhook = lambda value: (
	#     sys.__displayhook__(value)),
	#     _show_notification(etype, value, tb) if with_notification else None,
	#     _collect_exception(etype, value, tb) if with_collector else None,
	# )  # On exception: collect it, show notification

	sys.excepthook = lambda etype, value, tb: (
		sys.__excepthook__(etype, value, tb),
		_show_notification(etype, value, tb) if with_notification else None,
		_collect_exception(etype, value, tb) if with_collector else None,
	)  # On exception: collect it, show notification


def run_install_environment():
	"""Entry point to install missing environment. Only for developing purposes."""
	import subprocess
	import textwrap

	commands = textwrap.dedent("""
		sudo mkdir -p /tcos/home; sudo mount -o nolock,vers=2,rw,actimeo=0 $(mount | grep "/tcos/link" | head -n 1 | awk -F: '{{print $1}}'):/home /tcos/home
		sudo apt-get update
		sudo apt-get install -y python-qt4
	""".format(**locals()))
	for command in commands.split('\n'):
		try:
			logging.getLogger(__name__).warning('> %s', command)
			# process = subprocess.Popen(command, shell=True, stderr=sys.stderr)
			process = subprocess.Popen(command, shell=True)
			process.wait()
		except subprocess.CalledProcessError as e:
			logging.getLogger(__name__).warning('Command failed (exit code: %s): %s', e.returncode, e)


def run_simulate_exception():
	"""Entry point to simulate exception. Only for developing purposes."""
	import tcos_exception_hook
	tcos_exception_hook.init(
		# with_notification=False,
		# with_collector=False,
	)

	# Simulates exception
	nonsense


def run_proceed_external_exception():
	"""Entry point to receive exceptions from external (bash-)scripts (through STDIN)"""
	init(
		# with_notification=False,
		# with_collector=False,
	)

	# Receives a traceback-message from STDIN
	message = ''.join([line for line in sys.stdin])

	# Raises it as a python-exception
	try:
		raise Exception('abc')
	except Exception as e:
		sys.excepthook(sys.exc_info()[0], sys.exc_info()[1], message)


def _main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', required=True, choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	_main()
