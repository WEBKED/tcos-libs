#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:

"""Module to request for some system features
"""

from __future__ import division, unicode_literals; del division, unicode_literals  # Imports features but removes them from documentation
import logging
import os
import pipes
import re
import subprocess
import sys
import textwrap
import time

if __name__ == '__main__':
	# Sets utf-8 (instead of latin1) as default encoding for every IO
	reload(sys); sys.setdefaultencoding('utf-8')
	# Configures logging
	logging.basicConfig(level=logging.WARNING, datefmt='%H:%M:%S', format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s')

from helpers import cache


_default = object()  # For using as a default value for argument (instead of None if None has another meaning)


def run_command(command, stderr=_default, raise_exceptions=False, exit_on_exception=None, wait=True, detach=False):
	"""Runs shell command, returns its result

	Doc-Tests
	=========

		>>> run_command('echo test')
		'test\\n'

		>>> run_command('exit 1', raise_exceptions=True)  # doctest: +IGNORE_EXCEPTION_DETAIL
		Traceback (most recent call last):
		CalledProcessError: Command 'exit 1' returned non-zero exit status 1

		>>> run_command('exit 1', exit_on_exception=5)  # doctest: +IGNORE_EXCEPTION_DETAIL
		Traceback (most recent call last):
		SystemExit: 5

	"""
	logging.getLogger(__name__).debug('Command: %s', command)

	if detach:
		# See "double-fork"
		try:
			if os.fork():
				return  # First parent returns None

			os.chdir('/')
			os.setsid()
			os.umask(0)

			if os.fork():
				sys.exit(0)  # Second parent terminates
		except Exception as e:
			logging.getLogger(__name__).error('Fork failed: %s', e)

	try:
		if wait:
			result = subprocess.check_output(command, shell=True, stderr=(sys.stderr if stderr is _default else stderr))
		else:
			result = subprocess.Popen(command, shell=True, stderr=(sys.stderr if stderr is _default else stderr))
	except Exception as e:
		logging.getLogger(__name__).error('Exception: %s', e)
		if raise_exceptions:
			raise
		if exit_on_exception is not None:
			sys.exit(exit_on_exception)
	else:
		return result


def run_command_till_result(command, awaited_value=None, wait_timeout=10., delay_time=.2, raise_exceptions=False):
	"""Runs command multiple times (after delay) till some awaited value is not present in result, returns result or raises ValueError"""
	# Tries N times till awaited value is found
	for attempt in range(1, int(wait_timeout / delay_time) + 2):
		logging.getLogger(__name__).debug('Attempt %s to run "%s"...', attempt, command)

		result = run_command(command, raise_exceptions=raise_exceptions)
		if result is not None:
			if awaited_value is None or awaited_value in result:
				return result

		time.sleep(delay_time)
	raise ValueError('`{command}` failed during {wait_timeout}s.'.format(
		awaited_value_message=': awaited value "{awaited_value}" is not found.'.format(**locals()) if awaited_value is not None else '',
		**locals()
	))


def attempts(attempts=None, wait_timeout=None, delay_time=None):
	"""Runs something multiple times till max attempts or timeout reached.

	Doc-Tests
	=========

		>>> for attempt in attempts(3):
		...		print attempt,
		...		if attempt == 2:
		...			break
		... else:
		...		print 'failed',
		1 2

		>>> for attempt in attempts(3):
		...		print attempt,
		... else:
		...		print 'failed',
		1 2 3 failed

		>>> for attempt in attempts(wait_timeout=.1, delay_time=.01):  # doctest: +SKIP
		...		print attempt,
		... else:
		...		print 'failed',
		1 2 3 4 5 6 7 8 9 10 failed

	"""
	if attempts is None:
		attempts = int(wait_timeout / delay_time)

	# Tries N times till awaited value is found
	for attempt in range(1, attempts + 1):
		if attempt > 1:
			logging.getLogger(__name__).warning('Attempt %s from %s...', attempt, attempts)

		yield attempt

		if delay_time is not None:
			time.sleep(delay_time)


def decode(src, base=None, raise_exception=False):
	"""Tries to decode baseN-encoded value and returns it

	Doc-Tests
	=========

		>>> decode('dGVzdF8x', base=64)
		'test_1'

		>>> decode('74657374')
		'test'
		>>> map(decode, ['746573745F31', '746573745F32', 'something'])
		['test_1', 'test_2', 'something']

	"""
	import base64
	import binascii

	try:
		dst = (base64.b64decode if base == 64 else base64.b16decode)(src)
	except (TypeError, binascii.Error):  # Ignore if maybe not encoded
		if raise_exception:
			raise
		dst = src

	return dst


@cache.ExpiredMemoryCache(timeout=1)
def get_boot_arguments(path=None):
	"""Reads and parses boot arguments"""
	with open(path or '/proc/cmdline') as src:
		result = dict([(x.split('=', 1) + [True])[:2] for x in src.read().rstrip().split(' ') if x])
	return result


def run_get_boot_arguments():
	"""Entry point to check reading and parsing of boot arguments. Only for developing purposes."""
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, add_help=False)
	parser.add_argument('--path', help='Custom cmdline path to parse')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	result = get_boot_arguments(**kwargs)
	import json
	logging.getLogger(__name__).info('Result: (%s)\n%s', result.__class__.__name__, json.dumps(result, indent=4))


def get_default_ip_address():
	"""Requests for default (routed to server) IP-address, returns IP-address.

	TCOS_NIC -- interface to communicate with server (set in base/init)

	Doc-Tests
	=========

		>>> get_default_ip_address().split('.')  # doctest: +ELLIPSIS
		[...'...', ...'...', ...'...', ...'...']

	"""
	import netifaces

	try:
		default_interface = os.environ.get('TCOS_NIC', netifaces.gateways()[netifaces.AF_INET][0][1])
		default_ip_address = netifaces.ifaddresses(default_interface)[netifaces.AF_INET][0]['addr']
	except Exception:
		default_ip_address = None

	return default_ip_address


@cache.ExpiredMemoryCache(timeout=1)
def get_default_mac_address():
	"""Reads MAC address of default (routed to server) network interface

	TCOS_NIC -- interface to communicate with server (set in base/init)

	Doc-Tests
	=========

		>>> [get_default_mac_address()]  # doctest: +ELLIPSIS
		[...'...:...:...:...:...:...']

	"""
	import netifaces

	try:
		default_interface = os.environ.get('TCOS_NIC', netifaces.gateways()[netifaces.AF_INET][0][1])
		default_mac_address = netifaces.ifaddresses(default_interface)[netifaces.AF_LINK][0]['addr']
	except Exception:
		default_mac_address = None

	return default_mac_address


def get_screen_geometry():
	"""

	Doc-Tests
	=========

		>>> from helpers import tests

		>>> with tests.MockedSubprocessCheckOutput(map={
		...			'.*/xwininfo -root.*': '1024x768+0+0',
		...			'pidof mate-panel': None,
		...		}):
		...		get_screen_geometry()
		Command: /usr/bin/xwininfo -root | awk '/-geometry/ {print $2}'
		Command: pidof mate-panel
		u'1024x768+0+0'

		>>> with tests.MockedSubprocessCheckOutput(map={
		...			'.*/xwininfo -root.*': '800x600+100+200',
		...			'pidof mate-panel': '66666',
		...			'dconf read .*/auto_hide': '',
		...			'dconf read .*/size': '5',
		...		}):
		...		get_screen_geometry()
		Command: /usr/bin/xwininfo -root | awk '/-geometry/ {print $2}'
		Command: pidof mate-panel
		Command: dconf read /org/mate/panel/toplevels/top/auto_hide
		Command: dconf read /org/mate/panel/toplevels/top/size
		u'800x594+100+206'

	"""
	# Requests for screen geometry
	command = '/usr/bin/xwininfo -root | awk \'/-geometry/ {print $2}\''
	result = run_command(command)

	# Parses screen geometry
	width, height, x, y = (int(x) for x in re.search('([0-9]+)x([0-9]+)\+([0-9]+)\+([0-9]+)', result).groups())

	# Calculates geometry without panel
	if run_command('pidof mate-panel') and not run_command('dconf read /org/mate/panel/toplevels/top/auto_hide'):
		panel_height = int(run_command('dconf read /org/mate/panel/toplevels/top/size')) + 1
		y += panel_height
		height -= panel_height

	# Formats geometry string
	geometry = '{width}x{height}+{x}+{y}'.format(**locals())

	return geometry
getFullscreenDimensions = get_screen_geometry  # Previous (deprecated) function name


def get_screen_depth():
	"""

	Doc-Tests
	=========

		>>> from helpers import tests

		>>> with tests.MockedSubprocessCheckOutput(map={
		...			'.*/xwininfo -root.*': '16',
		...		}):
		...		get_screen_depth()
		Command: /usr/bin/xwininfo -root | awk '/Depth:/ {print $2}'
		'16'

	"""
	# Requests for screen geometry
	command = '/usr/bin/xwininfo -root | awk \'/Depth:/ {print $2}\''
	depth = run_command(command)

	return depth
getScreenDepth = get_screen_depth


def get_single_sign_on_password():
	"""Returns decrypted Single-Sign-On password"""
	password = None

	decryptor_path = '/usr/local/bin/sso-tcos-auth'
	if os.environ.get('TCOS_TOKEN', ''):  # Token to decrypt exists (used by decryption tool)
		if os.path.isfile(decryptor_path):
			with os.popen(decryptor_path) as src:
				password = src.read().rstrip('\r\n')

			if password == '""':
				password = ''

	return password


def dump_dictionary(data, delimiter='=', quote='"', style='key=value', with_file_modified_comment=True):
	"""Dumps dictionary into "key=value"-style (or INI-style if style='ini' was set)

	Doc-Tests
	=========

		>>> def test_dump_dictionary(**kwargs):
		...		for line in dump_dictionary(with_file_modified_comment=False, **kwargs).splitlines():
		...			print line

		>>> test_dump_dictionary(data=dict(a=False, b=True, c=2, d=3.14, e='some_value'))
		a="False"
		b="True"
		c="2"
		d="3.14"
		e="some_value"

		>>> test_dump_dictionary(style='ini', data={'a.a1': False, 'a.a2': True, 'b.b2': 2, 'b.b1': 3.14, 'b.b0': 'some_value'})
		<BLANKLINE>
		[a]
		a1=false
		a2=true
		<BLANKLINE>
		[b]
		b0="some_value"
		b1=3.14
		b2=2

	"""

	content = []

	if with_file_modified_comment:
		content += ['# Modified by {} at {}'.format(os.path.abspath(sys.argv[0]), time.asctime())]

	# Creates content in an INI-style
	if style.lower() == 'ini':
		previous_section = None
		for key, value in sorted(data.items()):
			section, key = key.split('.', 1)
			if section != previous_section:
				previous_section = section
				content += ['']
				content += ['[{section}]'.format(**locals())]

			if isinstance(value, bool):
				value = 'true' if value else 'false'
			elif isinstance(value, (str, unicode)):
				value = '"{}"'.format(value)

			content += ['{key}={value}'.format(**locals())]

	# Creates in a "key=value"-style
	else:
		for key, value in sorted(data.items()):
			if value is not None:
				content += ['{key}{delimiter}{quote}{value}{quote}'.format(**locals())]

	content = '\n'.join(content)

	return content


def show_popup(title=' ', message=''):
	"""Shows pop-up notification for every X-user"""
	command = textwrap.dedent('''
		ck-list-sessions |  # X-sessions
			awk -F"'" '/unix-user/{{print $2}}' |  # Ids of X-users
			xargs getent passwd | awk -F: '{{print $1}}' |  # Usernames of X-users
			xargs -I{{user}} sudo /bin/su {{user}} -c {command}  # Runs command from username
	'''.format(
		command=pipes.quote('''DISPLAY=:0 notify-send {title} {message}'''.format(
			title=pipes.quote(title),
			message=pipes.quote(message.replace('\\', '\\\\')),
		))
	))
	try:
		subprocess.Popen(command, shell=True, stderr=sys.stderr)  # Run & forget
	except subprocess.CalledProcessError as e:
		logging.getLogger(__name__).warning('Exception: %s', e)


def run_show_popup():
	"""Manual test for show_popup(...)"""
	show_popup(
		title='Test title',
		message=r'''Test message with specific characters: " \ ' .''',
	)


def _main():
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, add_help=False)
	parser.add_argument('-r', '--run-function', required=True, choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	_main()
