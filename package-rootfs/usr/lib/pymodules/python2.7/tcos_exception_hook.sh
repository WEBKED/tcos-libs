#!/bin/bash
#
# Implements tracebacks for a bash-script,
# sends exceptions to tcos_exception_hook.py
#
# *Uses bash traps*
#
# How to use it:
#	>>> . ./tcos_exception_hook.sh
#	(Insert it on the top of your bash-script)

set -o errexit  # Stops the script each time a command fails
#set -o nounset  # Stops if you attempt to use an undefined variable
set -o errtrace  # Propagates ERR trap handler functions, expansions and subshells
trap _show_traceback ERR  # Provides an error handler whenever a command exits nonzero
 
function _show_traceback() {
	local exit_code="$?"
	set +o xtrace

	# Creates traceback
	TRACEBACK=""
	if [ ${#FUNCNAME[@]} -gt 1 ]; then
		TRACEBACK+="Traceback (most recent call last):"
		for ((i = ${#FUNCNAME[@]} - 2; i > -1; i --)); do
			TRACEBACK+="\n  File \"${BASH_SOURCE[$i+1]}\", line ${BASH_LINENO[$i]}, in ${FUNCNAME[$i+1]}"
			TRACEBACK+="\n    "$(sed ${BASH_LINENO[$i]}'q;d' ${BASH_SOURCE[$i+1]} | awk '{$1=$1;print}')
		done
	fi
	TRACEBACK+="\nExited with status $exit_code"

	# Writes traceback
	>&2 echo -e "${TRACEBACK}"

	# Sends traceback into python's tcos_exception_hook
	echo -e "${TRACEBACK}" | $(dirname $0)/tcos_exception_hook.py -r proceed_external_exception
}

# Entry-point for tests. Only for developing purposes.
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
	function stack_bomb {
		#trap _errors_hook ERR
		local limit=${1:-5}
		echo -n " ${limit}"
		if [ "${limit}" -le 0 ]; then
			echo " BOOM"
			return 10
		else
			stack_bomb $(( ${limit} - 1 ))
		fi
	}

	function raise_traceback {
		subfunction
	}

	function subfunction {
		subsubfunction
	}

	function subsubfunction {
		unknown_function
	}

	case "${1:-}" in
		raise_traceback)
			raise_traceback
			;;
		unknown_function)
			unknown_function
			;;
		unknown_variable)
			echo "This shouldn't be shown because ${unknown_variable} isn't set"
			;;
		stack_bomb)
			stack_bomb
			;;
		false)
			false
			;;
		true)
			true
			;;
		*)
			clear

			for COMMAND in \
				raise_traceback \
				unknown_function \
				unknown_variable \
				stack_bomb \
				false \
				true \
				;
			do
				echo "CALLING ${COMMAND}..."
				./$0 ${COMMAND} && true
				echo "DONE ($?)"
				echo
			done
			;;
	esac
fi
