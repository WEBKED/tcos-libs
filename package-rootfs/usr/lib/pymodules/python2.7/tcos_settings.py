#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:

"""
Module to request for settings

Doc-Tests:

	# >>> get_settings_model

"""

from __future__ import division, unicode_literals; del division, unicode_literals  # Imports features but removes them from documentation
import json
import ldap
import logging
import os
import pipes
import socket
import subprocess
import sys
import urllib
import urllib3

if __name__ == '__main__':
	# Sets utf-8 (instead of latin1) as default encoding for every IO
	reload(sys); sys.setdefaultencoding('utf-8')
	# # Run in application's working directory
	# os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/.'); sys.path.insert(0, os.path.realpath(os.getcwd()))

# Configures logging
try:
	import tcos_logging
	tcos_logging.init(logger=logging.getLogger(__name__))
except ImportError:
	# Attention: does not redirect stderr
	logging.basicConfig(level=logging.WARNING, datefmt='%H:%M:%S', format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s')

from helpers import cache
from helpers.timer import Timer
import tcos_system


def _doctest_setup(namespace):
	"""Prepares each doc-tests environment for execution"""
	import mock; mock

	from helpers import tests; tests
	_schema_path = (os.path.dirname(sys.argv[-1]) or '.') + '/../../../../../'

	# Imports local variables into specified namespace
	namespace.update(locals())


class _HTTP(object):
	"""Class to request for HTTP-data"""
	pool = None

	@classmethod
	def fetch(cls, url):
		if cls.pool is None:
			# Sets default timeout for network requests
			socket.setdefaulttimeout(10)

			cls.pool = urllib3.PoolManager(
				timeout=5.,
				# cert_reqs=ssl.CERT_OPTIONAL,
				# cert_reqs=ssl.CERT_NONE,
			)

		try:
			with Timer('requesting for "{}" ({})'.format(url, sys.argv[0]), limit=.1):
				response = cls.pool.request('GET', url)
		except IOError as e:  # Skip if no connection
			logging.getLogger(__name__).error('No connection: %s', e)
			import tcos_notifier
			tcos_notifier.notify(
				wait=True,
				template='error',
				title='Error',
				en_message='No network',
				de_message='Kein Netzwerk',
			)
			raise

		# If request is failed and from OTC version <2.2
		if unicode(response.status)[0] in '345' and 'is not available' in response.data:
			raise urllib3.exceptions.HTTPError('HTTP response status {0.status}'.format(response))

		data = response.data

		return data


class _NetworkError(Exception):
	pass


def _get_default_settings_from_schema(schema_name, schema_path='/tcos/link/schema', **kwargs):
	from lxml import etree

	model = dict()

	# Reads XML-serialized schema
	try:
		command = 'grep -iRl --include={schema_name}.xml \\<schema\\[^\\>\\]\\*name=\\"{schema_name}\\" {schema_path}'.format(**locals())
		serialized_schema = tcos_system.run_command(command, raise_exceptions=True)
	except subprocess.CalledProcessError as e:
		logging.getLogger(__name__).error('Exception during reading of XML-serialized schema "{schema_name}": {e}. Ignoring.'.format(**locals()))
		# raise
		# import traceback
		# traceback.print_exception(*sys.exc_info())
	else:
		# Parses XML-serialized schema
		try:
			schema = etree.parse(serialized_schema.rstrip())
		except (IOError, etree.XMLSyntaxError) as e:
			logging.getLogger(__name__).error('Exception during parsing XML-serialized schema "{schema_name}: {e}"\nReturning configuration without defaults: {e}'.format(**locals()))
		else:
			# Appends keys from schema with found default values
			nodes = schema.xpath('//choice | //entry | //password')
			for node in nodes:
				key = '.'.join([parent.attrib['name'] for parent in node.xpath('ancestor-or-self::*')][1:])
				default = node.attrib.get('value', None)
				# Applies __BOOL and __INT suffixes
				for suffix, function in dict(
						__BOOL=lambda x: (bool(x.replace('false', '').replace('no', '').replace('off', ''))),
						__INT=int,
				).items():
					if key.endswith(suffix):
						key, default = key[:-len(suffix)], function(default)
				model[key] = default

	return model


def run_get_default_settings_from_schema():
	"""Entry point to request default settings model. Only for developing purposes."""
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, add_help=False)
	parser.add_argument('--schema-path', help='Custom path to schema directory')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	result = _get_default_settings_from_schema('hardwaretype', **kwargs)
	logging.getLogger(__name__).warning('Result: (%s)\n%s', result.__class__.__name__, json.dumps(result, indent=4))


def _get_settings_model_from_ldap(
		hardware=None,
		client=None,
		printer=None,
		device=None,
		application=None,
		server_address=None,
		username=None,
		with_defaults=False,
		**kwargs
):
	"""Uses LDAP-interface, returns dict-like object

	Doc-Tests
	=========

		>>> _doctest_setup(namespace=locals())

		>>> def test_get_settings_model_from_ldap(info={}, configuration={}, schema_path='', **kwargs):
		...		kwargs['schema_path'] = _schema_path + schema_path
		...		boot_arguments = dict(ldapurl='http://<ldapurl>')
		...		ldap = mock.Mock(name='ldap')
		...		tests.mock_modules('pytcos', 'pytcos.tcos')
		...		import pytcos.tcos
		...		pytcos.tcos.Ldap.return_value = tcos_ldap = mock.Mock(name='tcos_ldap')
		...		tcos_ldap.getHardwaretypesDn.return_value = ['hardwareDN1', 'hardwareDN2']
		...		tcos_ldap.getUserDn.return_value = 'userDN'
		...		tcos_ldap.getClientsDn.return_value = ['clientDN1', 'clientDN2']
		...		tcos_ldap.getPrintersDn.return_value = ['printerDN1', 'printerDN2']
		...		tcos_ldap.getDevicesDn.return_value = ['deviceDN1', 'deviceDN2']
		...		tcos_ldap.getAppsDn.return_value = ['applicationDN1', 'applicationDN2']
		...		tcos_ldap.getHardwaretypeInfo = tcos_ldap.getClientInfo = tcos_ldap.getGroupOfUniqueNamesInfo = lambda dn, url: dict(dict(name=(dn + '_name')), **info)
		...		tcos_ldap.getNismapentry = lambda dn, url: dict(dict(), **configuration)
		...		with \\
		...			tests.ReplacedObject(namespace=tcos_system, name='get_boot_arguments', replacement=lambda *args, **kwargs: boot_arguments), \\
		...			tests.ReplacedObject(namespace=__name__, name='ldap', replacement=ldap), \\
		...			tests.MockedOpen(namespace=__name__) as dst:
		...				model = _get_settings_model_from_ldap(**kwargs)
		...		return model

		# >>> test_get_settings_model_from_ldap(hardware='*', schema_path='../tcos-devices/schema')  # doctest: +ELLIPSIS
		# [{...'_name': 'hardwareDN1_name'...}, {...'_name': 'hardwareDN2_name'...}]
		# >>> test_get_settings_model_from_ldap(hardware='hardwareDN1_name', schema_path='../tcos-devices/schema')  # doctest: +ELLIPSIS
		# {...'_name': 'hardwareDN1_name'...}

		>>> test_get_settings_model_from_ldap(client='*', schema_path='../tcos-devices/schema')  # doctest: +ELLIPSIS
		[{...'_name': 'clientDN1_name'...}, {...'_name': 'clientDN2_name'...}]
		>>> test_get_settings_model_from_ldap(client='00:11:22:33:44:55', schema_path='../tcos-devices/schema', info=dict(macAddress='00:11:22:33:44:55'))  # doctest: +ELLIPSIS
		{...'_name': 'clientDN2_name'...}

		>>> test_get_settings_model_from_ldap(printer='*/*', schema_path='../cups-client/schema', info=dict(schema='cups-client'))  # doctest: +ELLIPSIS
		[{...'_name': 'printerDN1_name'...}, {...'_name': 'printerDN2_name'...}]
		>>> test_get_settings_model_from_ldap(printer='*/printerDN1_name', schema_path='../cups-client/schema', info=dict(schema='cups-client'))  # doctest: +ELLIPSIS
		{...'_name': 'printerDN1_name'...}

		>>> test_get_settings_model_from_ldap(device='*/*', schema_path='../tcos-devices/schema', info=dict(schema='autologin'))  # doctest: +ELLIPSIS
		[{u'_name': 'deviceDN1_name', u'_subtype': 'autologin'}, {u'_name': 'deviceDN2_name', u'_subtype': 'autologin'}]
		>>> test_get_settings_model_from_ldap(device='*/deviceDN1_name', schema_path='../tcos-devices/schema', info=dict(schema='autologin'))  # doctest: +ELLIPSIS
		{u'_name': 'deviceDN1_name', u'_subtype': 'autologin'}

		>>> test_get_settings_model_from_ldap(application='*/*', schema_path='../desktop/schema', info=dict(schema='desktop'))  # doctest: +ELLIPSIS
		[{...'_name': 'applicationDN1_name'...}, {...'_name': 'applicationDN2_name'...}]
		>>> test_get_settings_model_from_ldap(application='*/applicationDN1_name', schema_path='../desktop/schema', info=dict(schema='desktop'))  # doctest: +ELLIPSIS
		{...'_name': 'applicationDN1_name'...}

	"""
	if username is None:
		username = os.environ.get('USER', 'tcos')

	mac_address = client or tcos_system.get_default_mac_address()

	@_IgnoreExceptionsForLocalBootFilesystemCache(exceptions=_NetworkError, path=os.environ.get('LOCALBOOT_PATH', '/tcos/link') + '/var/cache/.tcos_settings_')
	def __get_settings_model_from_ldap(*_args, **_kwargs):
		import pytcos.tcos
		tcos_ldap = pytcos.tcos.Ldap()

		boot_arguments = tcos_system.get_boot_arguments(path=kwargs.get('cmdline_path', None))

		# Generates LDAP-URL
		if server_address is not None:
			# Only for developing purposes
			url = 'ldap://{server_address}:10389/ou%3Dopenthinclient%2Cdc%3Dopenthinclient%2Cdc%3Dorg????bindname=cn%3DroPrincipal%2Cou%3DRealmConfiguration%2Cou%3Dopenthinclient%2Cdc%3Dopenthinclient%2Cdc%3Dorg,X-BINDPW=c2VjcmV0'.format(**locals())
		else:
			# Attention: in case of localboot and if a local LDAP-server is running the server and port parts have to be replaced with ldap://127.0.0.1
			url = boot_arguments['ldapurl']
		logging.getLogger(__name__).debug('LDAP URL: %s', url)

		model = dict()

		# Checks LDAP-connection (throws exception if connection fails)
		from urlparse import urlparse
		try:
			ldap.initialize('{0.scheme}://{0.hostname}:{0.port}'.format(urlparse(url))).simple_bind_s()
			ldap.set_option(ldap.OPT_NETWORK_TIMEOUT, 10)

			# Requests for data from LDAP-server
			if hardware is not None:
				model = [
					dict(x, **dict(schema='hardwaretype', configuration=tcos_ldap.getNismapentry(hardwaretype_dn, url)))
					for hardwaretype_dn in tcos_ldap.getHardwaretypesDn(url)
					for x in [tcos_ldap.getHardwaretypeInfo(hardwaretype_dn, url)]
				]
			elif device is not None:
				pytcos.tcos.System.mac = mac_address  # Fix to set custom MAC address
				pytcos.tcos.System.username = username  # Fix to force user name
				model = [
					dict(x, **dict(subtype=x['schema'], configuration=tcos_ldap.getNismapentry(device_dn, url)))
					for device_dn in tcos_ldap.getDevicesDn(client_dn=tcos_ldap.getClientDn(mac_address, url), ldap_url=url)
					for x in [tcos_ldap.getGroupOfUniqueNamesInfo(device_dn, url)]
				]
			elif printer is not None:
				pytcos.tcos.System.mac = mac_address  # Fix to set custom MAC address
				pytcos.tcos.System.username = username  # Fix to force user name
				model = [
					dict(x, **dict(subtype=x['schema'], configuration=tcos_ldap.getNismapentry(printer_dn, url)))
					for printer_dn in tcos_ldap.getPrintersDn(client_dn=tcos_ldap.getClientDn(mac_address, url), user_dn=tcos_ldap.getUserDn(username, url), ldap_url=url)
					for x in [tcos_ldap.getGroupOfUniqueNamesInfo(printer_dn, url)]
				]
			elif application is not None:
				pytcos.tcos.System.mac = mac_address  # Fix to set custom MAC address
				pytcos.tcos.System.username = username  # Fix to force user name
				model = [
					dict(x, **dict(subtype=x['schema'], configuration=tcos_ldap.getNismapentry(application_dn, url)))
					for application_dn in tcos_ldap.getAppsDn(client_dn=tcos_ldap.getClientDn(mac_address, url), user_dn=tcos_ldap.getUserDn(username, url), ldap_url=url)
					for x in [tcos_ldap.getGroupOfUniqueNamesInfo(application_dn, url)]
				]
			elif client is not None:
				location_defaults = _get_default_settings_from_schema('location', **kwargs)
				model = [
					dict(x, **dict(schema='client', configuration=dict(tcos_ldap.getNismapentry(client_dn, url), **dict(location_defaults, **tcos_ldap.getNismapentry(tcos_ldap.getLocationsDn(client_dn, url), url)))))
					for client_dn in tcos_ldap.getClientsDn(url)
					for x in [tcos_ldap.getClientInfo(client_dn, url)]
				]
			else:
				raise KeyError('No hardware/client/printer/device/application specified.')
		except ldap.SERVER_DOWN as e:
			logging.getLogger(__name__).warning('Can not connect to URL "%s": %s', url, e)
			raise _NetworkError(e)

		return model
	model = __get_settings_model_from_ldap(
		_type='ldap',  # Key in order to differ, LDAP from REST (only for LDAP, REST does not have one)
		hardware=hardware is not None,
		client=(printer is not None or device is not None or application is not None) and mac_address or client is not None,
		printer=printer is not None,
		device=device is not None,
		application=application is not None,
	)
	if model is None:
		import tcos_notifier
		tcos_notifier.notify(
			wait=True,
			template='error',
			en_message='Local cache of settings empty',
			de_message='Locale Cache von Einstellungen ist leer',
		)
		sys.exit(-1)

	# Filters out or fills out data (by available arguments)
	if hardware is not None:
		if hardware != '*':
			# Looks for an item with specified name
			model = {x['name']: x for x in model}[hardware]
	elif printer is not None or device is not None or application is not None:
		subtype, name = (printer or device or application).split('/', 1)
		if subtype != '*' and name != '*':
			# Looks for an item with specified subtype and name
			model = {x['subtype'] + '/' + x['name']: x for x in model}[subtype + '/' + name]
		elif name != '*':
			# Looks for an item with specified name
			model = {x['name']: x for x in model}[name]
		else:
			# Filters out items with specified subtype and/or name
			model = [x for x in model if (subtype == '*' or x['subtype'] == subtype) and (name == '*' or x['name'] == name)]
			# Fills with defaults if empty (only if with_defaults is enabled)
			if not model and subtype != '*' and with_defaults:
				model.append(dict(subtype=subtype, schema=subtype, configuration={}))  # Will be filled with defaults later
	elif client is not None:
		if client != '*':
			model = {x['macAddress']: x for x in model}[client]

	# Transforms some keys (also to unify LDAP with REST)
	for item in ([model] if isinstance(model, dict) else model):
		if 'subtype' in item or 'name' in item:
			defaults = _get_default_settings_from_schema(item['schema'], **kwargs)
			item['configuration'] = dict(defaults, **item['configuration'])
			for key in [k for k in item if not k.startswith('_')]:
				item['_' + key] = item.pop(key)
			item.update(item.pop('_configuration', {}))
			for key in ['_schema', 'schema_version']:
				item.pop(key, None)

	# Orders items alphabetically
	if isinstance(model, list):
		model.sort(key=lambda x: (x.get('_subtype', None), x.get('_name', None)))

	return model


def _get_settings_model_from_rest(
		hardware=None,
		client=None,
		printer=None,
		device=None,
		application=None,
		server_address=None,
		server_port=8080,
		with_defaults=False,
		**kwargs
):
	"""Uses REST-interface, returns dict-like object.

	Urls
	====

	<hostname>:8080/ui/welcome
	<hostname>:8080/mappings/
	<hostname>:8080/api/v1/model/hardware-type/ (POST)

	Doc-Tests
	=========

		>>> _doctest_setup(namespace=locals())

		>>> def test_get_settings_model_from_rest(rest_data='', info={}, configuration={}, schema_path='', **kwargs):
		...		# kwargs['schema_path'] = _schema_path + schema_path
		...		boot_arguments = dict(resturl='REST_URL')
		...		_HTTP = mock.Mock(name='_HTTP')
		...		_HTTP.fetch = lambda url: rest_data[url]
		...		with \\
		...			tests.ReplacedObject(namespace=tcos_system, name='get_boot_arguments', replacement=lambda *args, **kwargs: boot_arguments), \\
		...			tests.ReplacedObject(namespace=__name__, name='_HTTP', replacement=_HTTP), \\
		...			tests.MockedOpen(namespace=__name__) as dst:
		...				model = _get_settings_model_from_rest(**kwargs)
		...		return model

		# >>> rest_data = {
		# ...		'REST_URL/model/hardware-type/': '["hw1","hw2"]',
		# ...		'REST_URL/model/hardware-type/hw1': '{"name":"hw1","type":"hardwaretype"}',
		# ...		'REST_URL/model/hardware-type/hw2': '{"name":"hw2","type":"hardwaretype"}',
		# ...	}
		# >>> test_get_settings_model_from_rest(hardware='*', schema_path='../tcos-devices/schema', rest_data=rest_data)  # doctest: +ELLIPSIS
		# [{...'_name': u'hw1'...}, {...'_name': u'hw2'...}]
		# >>> test_get_settings_model_from_rest(hardware='hw1', schema_path='../tcos-devices/schema', rest_data=rest_data)  # doctest: +ELLIPSIS
		# {...'_name': u'hw1'...}

		>>> rest_data = {
		...		'REST_URL/profiles/clients/': '[{"subtype":"client","name":"client1","macAddress":"00:11:22:33:44:55"},{"subtype":"client","name":"client2","macAddress":"11:22:33:44:55:66"}]',
		...		'REST_URL/profiles/clients/00%3A11%3A22%3A33%3A44%3A55': '{"subtype":"client","name":"client1","macAddress":"00:11:22:33:44:55"}',
		...	}
		>>> test_get_settings_model_from_rest(client='*', schema_path='../tcos-devices/schema', rest_data=rest_data)  # doctest: +ELLIPSIS
		[{...'_name': u'client1'...}, {...'_name': u'client2'...}]
		>>> test_get_settings_model_from_rest(client='00:11:22:33:44:55', schema_path='../tcos-devices/schema', rest_data=rest_data)  # doctest: +ELLIPSIS
		{...'_name': u'client1'...}

		>>> rest_data = {
		...		'REST_URL/profiles/clients/00%3A11%3A22%3A33%3A44%3A55/printers': '[{"subtype":"cups-client","name":"central cups server"},{"subtype":"printserver","name":"USB-printer"}]',
		...	}
		>>> test_get_settings_model_from_rest(client='00:11:22:33:44:55', printer='*/*', schema_path='../tcos-devices/schema', rest_data=rest_data)  # doctest: +ELLIPSIS
		[{...'_name': u'central cups server'...}, {...'_name': u'USB-printer'...}]
		>>> test_get_settings_model_from_rest(client='00:11:22:33:44:55', printer='*/central cups server', schema_path='../tcos-devices/schema', rest_data=rest_data)  # doctest: +ELLIPSIS
		{...'_name': u'central cups server'...}

		>>> rest_data = {
		...		'REST_URL/profiles/clients/00%3A11%3A22%3A33%3A44%3A55/devices': '[{"name":"device1"},{"name":"device2"}]',
		...	}
		>>> test_get_settings_model_from_rest(client='00:11:22:33:44:55', device='*/*', schema_path='../tcos-devices/schema', rest_data=rest_data)  # doctest: +ELLIPSIS
		[{...'_name': u'device1'...}, {...'_name': u'device2'...}]
		>>> test_get_settings_model_from_rest(client='00:11:22:33:44:55', device='*/device1', schema_path='../tcos-devices/schema', rest_data=rest_data)  # doctest: +ELLIPSIS
		{...'_name': u'device1'...}

		>>> rest_data = {
		...		'REST_URL/profiles/clients/00%3A11%3A22%3A33%3A44%3A55/applications': '[{"name":"application1"},{"name":"application2"}]',
		...	}
		>>> test_get_settings_model_from_rest(client='00:11:22:33:44:55', application='*/*', schema_path='../desktop/schema', rest_data=rest_data)  # doctest: +ELLIPSIS
		[{...'_name': u'application1'...}, {...'_name': u'application2'...}]
		>>> test_get_settings_model_from_rest(client='00:11:22:33:44:55', application='*/application1', schema_path='../desktop/schema', rest_data=rest_data)  # doctest: +ELLIPSIS
		{...'_name': u'application1'...}

	"""
	mac_address = client or tcos_system.get_default_mac_address()

	@_IgnoreExceptionsForLocalBootFilesystemCache(exceptions=_NetworkError, path=os.environ.get('LOCALBOOT_PATH', '/tcos/link') + '/var/cache/.tcos_settings_')
	def __get_settings_model_from_rest(*_args, **_kwargs):
		boot_arguments = tcos_system.get_boot_arguments(path=kwargs.get('cmdline_path', None))

		# Generates REST-URL
		if server_address is not None and server_port is not None:
			rest_url = 'http://{server_address}:{server_port}/api/v1'.format(**locals())
		else:
			rest_url = boot_arguments['resturl']

		# Selects URL (by available arguments)
		if hardware is not None:
			url = rest_url + '/model/hardware-type/{}'.format((urllib.quote(hardware) if hardware != '*' else ''))
		elif printer is not None:
			url = rest_url + '/profiles/clients/{}/printers'.format(urllib.quote(mac_address))
		elif device is not None:
			url = rest_url + '/profiles/clients/{}/devices'.format(urllib.quote(mac_address))
		elif application is not None:
			url = rest_url + '/profiles/clients/{}/applications'.format(urllib.quote(mac_address))
		elif client is not None:  # Follows printers, devices and applications
			url = rest_url + '/profiles/clients/{}'.format((urllib.quote(mac_address) if client != '*' else ''))
		else:
			raise KeyError('No hardware/client/printer/device/application specified.')
		logging.getLogger(__name__).debug('URL: %s', url)

		# Fetches data
		try:
			serialized_model = _HTTP.fetch(url=url)
		except (urllib3.exceptions.HTTPError, urllib3.exceptions.MaxRetryError) as e:
			logging.getLogger(__name__).error('Can not connect to URL "%s": %s', url, e)
			raise _NetworkError(e)
		logging.getLogger(__name__).debug('Serialized model: %s', serialized_model)

		# De-serializes data
		try:
			model = json.loads(serialized_model)
		except ValueError as e:
			raise KeyError('Exception during parsing response from "{}": {}'.format(url, e))

		# Fills out data (by available arguments)
		if hardware is not None:
			if hardware == '*':
				# Fills out list of hardwares (instead of list of their names)
				model = [_get_settings_model_from_rest(hardware=name, server_address=server_address, server_port=server_port, **kwargs) for name in model]

		return model
	model = __get_settings_model_from_rest(
		hardware=hardware is not None,
		client=(printer is not None or device is not None or application is not None) and mac_address or client is not None and client,
		printer=printer is not None,
		device=device is not None,
		application=application is not None,
	)
	if model is None:
		import tcos_notifier
		tcos_notifier.notify(
			wait=True,
			template='error',
			en_message='Local cache of settings empty',
			de_message='Locale Cache von Einstellungen ist leer',
		)
		sys.exit(-1)

	# Filters out or fills out data (by available arguments)
	if hardware is not None:
		pass
	elif printer is not None or device is not None or application is not None:
		subtype, name = (printer or device or application).split('/', 1)
		if subtype != '*' and name != '*':
			# Looks for an item with specified subtype and name
			model = {x['subtype'] + '/' + x['name']: x for x in model}[subtype + '/' + name]
		elif name != '*':
			# Looks for an item with specified name
			model = {x['name']: x for x in model}[name]
		else:
			# Filters out items with specified subtype and/or name
			model = [x for x in model if (subtype == '*' or x['subtype'] == subtype) and (name == '*' or x['name'] == name)]
			# Fills with defaults if empty (only if with_defaults is enabled)
			if with_defaults and not model and subtype != '*':
				model.append(dict(_subtype=subtype, **_get_default_settings_from_schema(subtype, **kwargs)))
	elif client is not None:  # Follows printers, devices and applications
		if client != '*':
			# Checks if model contains client with specified MAC-address
			model = {model.get('macAddress', None): model}[client]

	# Transforms some keys (also to unify LDAP with REST)
	for item in ([model] if isinstance(model, dict) else model):
		if 'subtype' in item or 'name' in item:
			for key in [k for k in item if not k.startswith('_')]:
				item['_' + key] = item.pop(key)
			item.update(item.pop('_configuration', {}))
			for key in ['_members']:
				item.pop(key, None)
			# Applies __BOOL and __INT suffixes
			for key, value in item.items():
				for suffix, function in dict(
						__BOOL=lambda x: (bool(x.replace('false', '').replace('no', '').replace('off', ''))),
						__INT=int,
				).items():
					if key.endswith(suffix):
						item.pop(key)
						key, value = key[:-len(suffix)], function(value)
						item[key] = value

	# Orders items alphabetically
	if isinstance(model, list):
		model.sort(key=lambda x: (x.get('_subtype', None), x.get('_name', None)))

	return model


class _IgnoreExceptionsForLocalBootFilesystemCache(cache.IgnoreExceptionsFilesystemCache):
	"""Ignores exceptions only if booted locally"""
	def _save(self, path, value):
		"""Forces partition to be temporarily read-write"""
		for attempt in tcos_system.attempts(5):  # N attempts to save cache
			try:
				if 'LOCALBOOT_PATH' not in os.environ:
					tcos_system.run_command('''mount -o remount,rw /tcos/link || sudo mount -o remount,rw /tcos/link''', raise_exceptions=True)
				tcos_system.run_command('''(mkdir -p {0} || sudo mkdir -p {0}) && (chmod 0777 {0} || sudo chmod 0777 {0})'''.format(pipes.quote(os.path.dirname(self._path))), raise_exceptions=True)
				try:
					super(_IgnoreExceptionsForLocalBootFilesystemCache, self)._save(path, value)
				finally:
					if 'LOCALBOOT_PATH' not in os.environ:
						tcos_system.run_command('mount -o remount,ro /tcos/link || sudo mount -o remount,ro /tcos/link', raise_exceptions=True)
				break
			except Exception as e:
				pass  # Re-tries if exception occured
		else:
			logging.getLogger(__name__).error('Failed to save cache: %s', e)

	def _called(self, *args, **kwargs):
		"""Uses cache only if booted locally"""
		cmdline_path = kwargs.get('cmdline_path', None)
		boot_arguments = tcos_system.get_boot_arguments(path=cmdline_path)

		if 'localboot' in os.environ or 'localboot' in boot_arguments:
			value = super(_IgnoreExceptionsForLocalBootFilesystemCache, self)._called(*args, **kwargs)
		else:
			for attempt in tcos_system.attempts(3):  # N attempts to fetch response
				try:
					value = self._function(*args, **kwargs)
				except _NetworkError as e:
					logging.getLogger(__name__).error('Exception: %s', e)
					continue
				break
			else:
				import tcos_notifier
				tcos_notifier.notify(
					wait=True,
					template='error',
					en_message='Server connection timeout',
					de_message='Zeitlimit für Serververbindung',
				)
				raise e

		return value


# @_IgnoreExceptionsForLocalBootFilesystemCache(exceptions=_NetworkError, path='/tcos/link/var/cache/.tcos_settings_')
def get_settings_model(*args, **kwargs):
	"""Requests for settings, returns dict-like (or list-like if "*" found) object.

	Attention! Applications have to decode theirs first argument (title):

		>>> import tcos_system

		>>> # Replaces base16-encoded argument with the decoded one
		>>> sys.argv[1:2] = map(tcos_system.decode, sys.argv[1:2])


	How to migrate from pytcos.tcos to tcos_settings:

		>>> import pytcos.tcos  # doctest: +SKIP
		... settings_model = pytcos.tcos.Launcher().ENTRY

		replace with (typical usage for applications):

		>>> try:  # doctest: +SKIP
		... 	import tcos_settings
		... 	settings_model = tcos_settings.get_settings_model(application='<application_type>/' + sys.argv[1].split(',', 1)[0].split('=', 1)[-1])
		... except (ImportError, IndexError):
		...		settings_model = {}

		or (typical usage for devices):

		>>> try:  # doctest: +SKIP
		...		import tcos_settings
		...		settings_models = tcos_settings.get_settings_model(device='usb-manager/*')
		... except ImportError:
		...		settings_models = []
		...
		... if not settings_models:
		...		logging.getLogger(__name__).warning('No usb-manager device found, doing nothing.')
		... else:
		...		settings_model=settings_models[0]
		...		# Do something ...

		or (typical usage for printers):

		>>> try:  # doctest: +SKIP
		...		import tcos_settings
		...		settings_models = tcos_settings.get_settings_model(printer='cups-client/*')
		... except ImportError:
		...		settings_models = []
		...
		... for settings_model in settings_models:
		...		# Do something ...

	"""

	model = dict()

	# Extracts REST URL from kernel cmdline (if available)
	boot_arguments = tcos_system.get_boot_arguments(path=kwargs.get('cmdline_path', None))
	rest_url = boot_arguments.get('resturl', None)
	ldap_url = boot_arguments.get('ldapurl', None)

	# Fetches model from REST (or LDAP)
	if rest_url is not None:
		model = _get_settings_model_from_rest(*args, **kwargs)
	elif ldap_url is not None:
		model = _get_settings_model_from_ldap(*args, **kwargs)

	return model


def run_get_settings_model():
	"""Entry point to request settings model. Only for developing purposes.

	Examples
	========

		> ./tcos_settings.py -r get_settings_model --method rest --server-address otc-server-2-2 --hardware "*"
		> ./tcos_settings.py -r get_settings_model --method rest --server-address otc-server-2-2 --hardware "(default)"
		> ./tcos_settings.py -r get_settings_model --method rest --server-address otc-server-2-2 --client "*"
		> ./tcos_settings.py -r get_settings_model --method rest --server-address otc-server-2-2 --client "00:e0:c5:49:61:da"
		> ./tcos_settings.py -r get_settings_model --method rest --server-address otc-server-2-2 --client "00:e0:c5:49:61:da" --printer "*/*"
		> ./tcos_settings.py -r get_settings_model --method rest --server-address otc-server-2-2 --client "00:e0:c5:49:61:da" --device "*/*"
		> ./tcos_settings.py -r get_settings_model --method rest --server-address otc-server-2-2 --client "00:e0:c5:49:61:da" --application "*/*"

	"""
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, add_help=False)
	parser.add_argument('--method', choices=['ldap', 'rest', 'cache'], help='Database access method')
	parser.add_argument('--cmdline-path', help='Custom cmdline path to parse')
	parser.add_argument('--schema-path', help='Custom path to schema directory')
	parser.add_argument('--server-address', help='Custom server address')
	parser.add_argument('--server-port', help='Custom server port')
	parser.add_argument('--username', help='Custom username')
	parser.add_argument('--hardware', help='Custom hardware name or "*"')
	parser.add_argument('--client', help='Custom client MAC address or "*"')
	parser.add_argument('--printer', help='Custom printer, format: (<subtype>|*)/(<name>|*)')
	parser.add_argument('--device', help='Custom device, format: (<subtype>|*)/(<name>|*)')
	parser.add_argument('--application', help='Custom application, format: (<subtype>|*)/(<name>|*)')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Selects function to use
	function = dict(
		rest=_get_settings_model_from_rest,
		ldap=_get_settings_model_from_ldap,
		# cache=cache.IgnoreExceptionsFilesystemCache(function=get_settings_model, exceptions=_NetworkError, path='./.tcos_settings_cache_'),
	).get(kwargs.pop('method', None), get_settings_model)
	# function = get_settings_model

	result = function(**kwargs)
	logging.getLogger(__name__).info('Result: (%s)\n%s', result.__class__.__name__, json.dumps(result, indent=4))


def run_get_settings_models():
	"""Entry point to request settings models. Only for developing purposes."""
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, add_help=False)
	parser.add_argument('--method', choices=['ldap', 'rest', 'cache'], help='Database access method')
	parser.add_argument('--cmdline-path', help='Custom cmdline path to parse')
	parser.add_argument('--schema-path', help='Custom path to schema directory')
	parser.add_argument('--server-address', help='Custom server address')
	parser.add_argument('--server-port', help='Custom server port')
	parser.add_argument('--username', help='Custom username')
	parser.add_argument('--hardware', help='Custom hardware name or "*"')
	parser.add_argument('--client', help='Custom client MAC address or "*"')
	parser.add_argument('--printer', help='Custom printer, format: (<subtype>|*)/(<name>|*)')
	parser.add_argument('--device', help='Custom device, format: (<subtype>|*)/(<name>|*)')
	parser.add_argument('--application', help='Custom application, format: (<subtype>|*)/(<name>|*)')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	# Selects function to use
	function = dict(
		rest=_get_settings_model_from_rest,
		ldap=_get_settings_model_from_ldap,
		cache=cache.IgnoreExceptionsFilesystemCache(function=get_settings_model, exceptions=_NetworkError, path='./.tcos_settings_cache_'),
	).get(kwargs.pop('method', None), get_settings_model)

	logging.getLogger(__name__).info('Hardware types')
	logging.getLogger(__name__).info('==============')
	for hardware in function(hardware='*', **kwargs):
		logging.getLogger(__name__).info('Hardware: NAME="{hardware[_name]}"'.format(**locals()))
		# logging.getLogger(__name__).info('Hardware: (%s)\n%s', hardware.__class__.__name__, json.dumps(hardware, indent=4))
	logging.getLogger(__name__).info('')

	logging.getLogger(__name__).info('Hardware type "(default)"')
	logging.getLogger(__name__).info('=========================')
	hardware = function(hardware='default - 64 bit', **kwargs)
	logging.getLogger(__name__).info('Hardware: NAME="{hardware[_name]}"'.format(**locals()))
	# logging.getLogger(__name__).info('Hardware: (%s)\n%s', hardware.__class__.__name__, json.dumps(hardware, indent=4))
	logging.getLogger(__name__).info('')

	logging.getLogger(__name__).info('Clients')
	logging.getLogger(__name__).info('=======')
	clients = function(client='*', **kwargs)
	clients = sorted(clients, key=lambda x: x['_name'], reverse=True)
	if not clients:
		logging.getLogger(__name__).error('No clients found!')
		logging.getLogger(__name__).error('Please add at least one client.')
	for client in clients:
		# logging.getLogger(__name__).info('Client: (%s)\n%s', client.__class__.__name__, json.dumps(client, indent=4))
		logging.getLogger(__name__).info('Client: NAME="{client[_name]}" MAC="{client[_macAddress]}""'.format(**locals()))
	logging.getLogger(__name__).info('')

	mac_address = kwargs.get('client', clients[0]['_macAddress'])
	if mac_address:
		logging.getLogger(__name__).info('Client (the first one)')
		logging.getLogger(__name__).info('======================')
		client = function(client=mac_address, **kwargs)
		logging.getLogger(__name__).info('Client: NAME="{client[_name]}" MAC="{client[_macAddress]}"'.format(**locals()))
		# logging.getLogger(__name__).info('Client: (%s)\n%s', client.__class__.__name__, json.dumps(client, indent=4))
		logging.getLogger(__name__).info('')

		logging.getLogger(__name__).info('Devices')
		logging.getLogger(__name__).info('=======')
		devices = function(device='*/*', client=mac_address, **kwargs)
		for device in devices:
			logging.getLogger(__name__).info('Device: SUBTYPE="{device[_subtype]}" NAME="{device[_name]}"'.format(**locals()))
			# logging.getLogger(__name__).info('Device: (%s)\n%s', device.__class__.__name__, json.dumps(device, indent=4))
		logging.getLogger(__name__).info('')

		logging.getLogger(__name__).info('Device <autologin>')
		logging.getLogger(__name__).info('==================')
		devices = function(device='autologin/*', client=mac_address, **kwargs)
		for device in devices:
			logging.getLogger(__name__).info('Device: SUBTYPE="{device[_subtype]}" NAME="{device[_name]}"'.format(**locals()))
			# logging.getLogger(__name__).info('Device: (%s)\n%s', device.__class__.__name__, json.dumps(device, indent=4))
		logging.getLogger(__name__).info('')

		logging.getLogger(__name__).info('Device <keyboard>')
		logging.getLogger(__name__).info('=================')
		device = function(device='keyboard/Keyboard: North America (EN-US)', client=mac_address, **kwargs)
		logging.getLogger(__name__).info('Device: SUBTYPE="{device[_subtype]}" NAME="{device[_name]}"'.format(**locals()))
		# logging.getLogger(__name__).info('Device: (%s)\n%s', device.__class__.__name__, json.dumps(device, indent=4))
		logging.getLogger(__name__).info('')

		logging.getLogger(__name__).info('Printers')
		logging.getLogger(__name__).info('========')
		printers = function(printer='*/*', client=mac_address, **kwargs)
		for printer in printers:
			logging.getLogger(__name__).info('Printer: SUBTYPE="{printer[subtype]}" NAME="{printer[name]}"'.format(**locals()))
			# logging.getLogger(__name__).info('Printer: (%s)\n%s', printer.__class__.__name__, json.dumps(printer, indent=4))
		logging.getLogger(__name__).info('')

		# logging.getLogger(__name__).info('Printer "Default printer"')
		# logging.getLogger(__name__).info('=========================')
		# printer = function(printer='*/Default printer', client=mac_address, **kwargs)
		# for printer in printers:
		#     logging.getLogger(__name__).info('Printer: SUBTYPE="{printer[_subtype]}" NAME="{printer[_name]}"'.format(**locals()))
		#     # logging.getLogger(__name__).info('Printer: (%s)\n%s', printer.__class__.__name__, json.dumps(printer, indent=4))
		# logging.getLogger(__name__).info('')

		logging.getLogger(__name__).info('Applications')
		logging.getLogger(__name__).info('============')
		applications = function(application='*/*', client=mac_address, **kwargs)
		for application in applications:
			# logging.getLogger(__name__).info('Application: (%s)\n%s', application.__class__.__name__, json.dumps(application, indent=4))
			logging.getLogger(__name__).info('Application: SUBTYPE="{application[_subtype]}" NAME="{application[_name]}"'.format(**locals()))
			# logging.getLogger(__name__).info('Application: SUBTYPE="{application[subtype]}" NAME="{application[name]}" SETTINGS={settings}'.format(settings=json.dumps(application['configuration'], indent=4), **locals()))
		logging.getLogger(__name__).info('')

		logging.getLogger(__name__).info('Application <browser>')
		logging.getLogger(__name__).info('=====================')
		applications = function(application='browser/*', client=mac_address, **kwargs)
		for application in applications:
			logging.getLogger(__name__).info('Application: SUBTYPE="{application[_subtype]}" NAME="{application[_name]}"'.format(**locals()))
			# logging.getLogger(__name__).info('Application: SUBTYPE="{application[subtype]}" NAME="{application[name]}" SETTINGS={settings}'.format(settings=json.dumps(application['configuration'], indent=4), **locals()))
		logging.getLogger(__name__).info('')

		# Requests for client applications
		logging.getLogger(__name__).info('Application "default browser"')
		logging.getLogger(__name__).info('=============================')
		application = function(application='browser/Kioskbrowser - right display', client=mac_address, **kwargs)
		logging.getLogger(__name__).info('Application: SUBTYPE="{application[_subtype]}" NAME="{application[_name]}"'.format(**locals()))
		# logging.getLogger(__name__).info('Application: SUBTYPE="{application[subtype]}" NAME="{application[name]}" SETTINGS={settings}'.format(settings=json.dumps(application['configuration'], indent=4), **locals()))
		logging.getLogger(__name__).info('')


def _main():
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, add_help=False)
	parser.add_argument('-r', '--run-function', required=True, choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	_main()
