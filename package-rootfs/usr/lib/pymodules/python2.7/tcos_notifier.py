#!/bin/sh
# -*- coding: utf-8 -*-
# vim: noexpandtab
"exec" "python" "-B" "$0" "$@"

from __future__ import division, unicode_literals; del division, unicode_literals  # Imports features but removes them from documentation
import ast
import contextlib
import logging
import os
import pipes
import subprocess
import sys
import textwrap

__doc__ = """
Module to notify user in different ways (pop-up window, balloon message, etc.).

How to use it
=============

Insert:
	>>> import tcos_notifier

into your script's section where imports of 3rd-party modules are.


Dependencies
============

* package 'python-qt4'
* symbolic link '/usr/bin/mate-open' => '/usr/bin/gnome-open'
"""

# Configures logging
try:
	import tcos_logging
	tcos_logging.init(logger=logging.getLogger(__name__))
except ImportError:
	# Attention: does not redirect stderr
	logging.basicConfig(level=logging.WARNING, datefmt='%H:%M:%S', format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s')


def _doctest_setup(namespace):
	"""Prepares each doc-tests environment for execution"""
	import mock; mock

	from helpers import tests; tests

	# Imports local variables into specified namespace
	namespace.update(locals())


def notify(
		template=None,
		size=None,
		title=None,
		message=None,
		en_message=None,
		de_message=None,
		with_close_on_escape=None,
		close_after=None,
		wait=False,
		stdin=False,
):
	"""Shows styled OTC notification.

	Arguments (for defaults see /opt/tcos-notifier/controllers/notifier.py:NotifierController.__init__):
		template (string) -- template name (from /opt/tcos-notifier/templates/)
		size (string) -- Default window size. Can be small, medium, large and xlarge
		title (string) -- Sets window title (not a message title)
		message, en_message, de_message (string) -- message or localized messages (message has priority)
		with_close_on_escape (bool) -- allows window close on ESC button
		close_after (float) -- sets window auto-close timeout (in seconds)
		wait (bool) -- waits for a user confirmation (or runs & forgets). Default: False.
		stdin (bool) -- opens stdin-pipe, returns function to send (serialized) data into process

	Returns dict-like object or None.

	Doc-Tests
	=========

		>>> _doctest_setup(namespace=locals())

		>>> def test_notify(logging_level=None, **kwargs):
		...		logging.getLogger(__name__).setLevel(logging_level or logging.WARNING)
		...		with tests.MockedSubprocessPopen(), \\
		...			tests.MockedSubprocessCheckOutput(map={'tcos-notifier': '{"result": "1"}'}):
		...			return notify(**kwargs)

		>>> test_notify(logging_level=logging.INFO)
		Command: tcos-notifier -v

		>>> test_notify(logging_level=logging.DEBUG)
		Command: tcos-notifier -v -v

		>>> test_notify()
		Command: tcos-notifier

		>>> test_notify(template='error')
		Command: tcos-notifier --template=error

		>>> test_notify(template='error', message='<h5>Some message</h5>')
		Command: tcos-notifier --template=error --message='<h5>Some message</h5>'

		>>> test_notify(template='error', en_message='<h5>Some message</h5>')
		Command: tcos-notifier --template=error --en-message='<h5>Some message</h5>'

		>>> test_notify(template='error', en_message='<h5>Some message</h5>', de_message='<h5>Einige Mitteilung</h5>')
		Command: tcos-notifier --template=error --en-message='<h5>Some message</h5>' --de-message='<h5>Einige Mitteilung</h5>'

		>>> test_notify(with_close_on_escape=False)
		Command: tcos-notifier --with-close-on-escape=no

		>>> test_notify(with_close_on_escape=True)
		Command: tcos-notifier --with-close-on-escape=yes

		>>> test_notify(close_after=5.)
		Command: tcos-notifier --close-after=5.0

		>>> test_notify(wait=True)
		Command: tcos-notifier
		{'result': '1'}

		>>> test_notify(stdin=True)  # doctest: +ELLIPSIS
		Command: exec tcos-notifier
		(<function send_into_stdin at 0x...>, <function close at 0x...>)

	"""
	command = 'tcos-notifier'
	command += (' -v' * {logging.INFO: 1, logging.DEBUG: 2}.get(logging.getLogger(__name__).level, 0))  # Sends the same verbosity level as in the main process
	if template is not None:
		command += ' --template={}'.format(pipes.quote(textwrap.dedent(template)))
	if size is not None:
		command += ' --size={}'.format(pipes.quote(textwrap.dedent(size)))
	if title is not None:
		command += ' --title={}'.format(pipes.quote(textwrap.dedent(title)))
	if message is not None:
		command += ' --message={}'.format(pipes.quote(textwrap.dedent(message)))
	if en_message is not None:
		command += ' --en-message={}'.format(pipes.quote(textwrap.dedent(en_message)))
	if de_message is not None:
		command += ' --de-message={}'.format(pipes.quote(textwrap.dedent(de_message)))
	if with_close_on_escape is not None:
		command += ' --with-close-on-escape={}'.format(pipes.quote('yes' if with_close_on_escape else 'no'))
	if close_after is not None:
		command += ' --close-after={}'.format(close_after)

	logging.getLogger(__name__).debug('Command: %s', command)

	if stdin:  # Runs & sends data into stdin-pipe
		process = subprocess.Popen('exec ' + command.encode('UTF-8'), shell=True, stdin=subprocess.PIPE, stderr=sys.stderr, preexec_fn=os.setpgrp)  # Exec replaces sub-shell (fixes process.terminate())

		def close():
			# Closes stdin-pipe
			process.stdin.close()

			try:
				# Sends SIGTERM
				process.terminate()
			except OSError:  # Ignores if process has been already terminated
				pass

			# Gets exit code
			process.wait()

		def send_into_stdin(**kwargs):
			"""Sends serialized data into subprocess via stdin-pipe, terminates subprocess if no data"""
			try:
				# Stops subprocess if no keyword arguments
				if not kwargs:
					raise StopIteration()  # Terminates communication & process

				# Sends data into subprocess
				logging.getLogger(__name__).debug('Sending data into subprocess: %s', kwargs)
				process.stdin.write(unicode(kwargs) + '\n')

			except (ValueError, IOError, StopIteration) as e:
				if isinstance(e, IOError):
					import errno
					if e.errno not in (errno.EPIPE, errno.EINVAL):  # If not invalid pipe or invalid argument
						raise
					logging.getLogger(__name__).error('%s: %s', e.__class__, e)
				close()

		# Returns functions to send data and to close process
		return (send_into_stdin, close)

	elif wait:  # Waits here till the dialog is closed
		try:
			result = subprocess.check_output(command.encode('UTF-8'), shell=True, stderr=sys.stderr)
		except Exception as e:
			logging.getLogger(__name__).error('Command "%s" failed: %s', command, e)
			raise
		else:
			# Tries to parse python object from data
			try:
				result = ast.literal_eval(result)
			except SyntaxError:
				pass
			else:
				return result
	else:  # Runs & forgets
		try:
			subprocess.Popen(command.encode('UTF-8'), shell=True, stderr=sys.stderr)
		except Exception as e:
			logging.getLogger(__name__).error('Command "%s" failed: %s', command, e)
			raise


def run_show_exception_notification_example():
	"""Entry point to show notification example. Only for developing purposes."""
	result = notify(wait=True, template='exception', title='Exception', en_message='Exception', de_message='Ausnahmefall')
	logging.getLogger(__name__).warning('Result: (%s) "%s"', result.__class__.__name__, result)


def run_show_login_notification_example():
	"""Entry point to show notification example. Only for developing purposes."""
	result = notify(wait=True, template='login', title='Login', en_message='<h2>Test login</h2>', de_message='<h2>Test Einloggen</h2>')
	logging.getLogger(__name__).warning('Result: (%s) "%s"', result.__class__.__name__, result)


@contextlib.contextmanager
def Progress(title='Processing...', en_message='Processing...', de_message='Im Prozeß...', size='xsmall', **kwargs):
	"""Context to operate with progress dialog, returns function to update dialog"""
	# Shows progress
	(update, close) = notify(stdin=True, wait=True, template='progress', size=size, title=title, en_message=en_message, de_message=de_message, with_close_on_escape=False, **kwargs)

	try:
		# Returns function to update progress
		yield update

	finally:
		# Hides progress
		close()


def run_show_progress_example():
	"""Entry point to show progress example. Only for developing purposes."""
	import time
	with Progress() as progress:
		time.sleep(1)
		progress(en_message='Test #1', de_message='Test ♯1')
		time.sleep(1)
		progress(en_message='Test #2', de_message='Test ♯2')
		time.sleep(1)


def show_balloon_message(title, en_message, de_message):
	"""Shows pop-up notification for every X-user"""
	command = textwrap.dedent('''
		ck-list-sessions |  # X-sessions
			awk -F"'" '/unix-user/{{print $2}}' |  # Ids of X-users
			xargs getent passwd | awk -F: '{{print $1}}' |  # Usernames of X-users
			xargs -I{{user}} sudo /bin/su {{user}} -c {command}  # Runs command from username
	'''.format(
		command=pipes.quote('''
			if test "${{LANG:0:2}}" == "de"; then
				DISPLAY=:0 notify-send {title} {de_message};
			else
				DISPLAY=:0 notify-send {title} {en_message};
			fi
		'''.format(
			title=pipes.quote(title),
			en_message=pipes.quote(en_message),
			de_message=pipes.quote(de_message),
		))
	))
	logging.getLogger(__name__).debug('Command: %s', command)
	subprocess.Popen(command, shell=True, stderr=sys.stderr)  # Runs & forgets


def run_show_balloon():
	"""Entry point to show balloon"""
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, description=__doc__)
	parser.add_argument('--title', type=unicode, required=True, help='Balloon title')
	parser.add_argument('--en-message', type=unicode, required=True, help='Balloon body (EN)')
	parser.add_argument('--de-message', type=unicode, required=True, help='Balloon body (DE)')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	show_balloon_message(**kwargs)


def run_show_balloon_example():
	"""Entry point to show balloon example. Only for developing purposes."""
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, description=__doc__)
	parser.add_argument('--title', type=unicode, default='Balloon title', help='Balloon title')
	parser.add_argument('--en-message', type=unicode, default='Balloon message (EN)', help='Balloon body (EN)')
	parser.add_argument('--de-message', type=unicode, default='Balloon message (DE)', help='Balloon body (DE)')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	show_balloon_message(**kwargs)


def _main():
	import argparse
	parser = argparse.ArgumentParser(add_help=False)
	parser.add_argument('-r', '--run-function', required=True, choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	_main()
