#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:

"""Module to provide tools in order to create unit-tests
"""

from __future__ import division, unicode_literals; del division, unicode_literals  # Imports features but removes them from documentation
import ast
import glob
import json
import logging
import os
import sys
import textwrap

if __name__ == '__main__':
	# Sets utf-8 (instead of latin1) as default encoding for every IO
	reload(sys); sys.setdefaultencoding('utf-8')
	# Configures logging
	logging.basicConfig(level=logging.WARNING, datefmt='%H:%M:%S', format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s')


def _extend_builtin_reload(__state=dict(extended=False)):
	"""Extends __builtin__.reload with reloading of modules with custom extensions (they have to implement __reload__-function)"""
	if not __state['extended']:
		__state['extended'] = True

		previous = sys.modules['__builtin__'].reload

		def wrapper(module):
			if hasattr(module, '__reload__'):
				module = module.__reload__(module)
			else:
				module = previous(module)
			return module
		sys.modules['__builtin__'].reload = wrapper


def import_file(namespace, path, name=None):
	"""Imports python modules with custom file extensions

	Example:

		>>> import tempfile
		>>> with tempfile.NamedTemporaryFile() as src:
		...		module = type(b'Module', (object, ), {})()
		...		module.a = 1
		...		# Loads module with a custom file extension (without file extension)
		...		src.write('"Test module"\\n\\nb = 2\\n')
		...		src.flush()
		...		src.seek(0)
		...		import_file(namespace=module, path=src.name)
		...		[(k, v) for k, v in sorted(module.__dict__.items()) if not k.startswith('_')]
		...		# Checks if 'reload' works right
		...		src.write('"Test module"\\n\\nc = 3\\n')
		...		src.seek(0)
		...		_ = reload(module)
		...		[(k, v) for k, v in sorted(module.__dict__.items()) if not k.startswith('_')]
		...		# doctest: +ELLIPSIS
		[('a', 1), ('b', 2)]
		[('a', 1), ('b', 2), ('c', 3)]

	"""
	if isinstance(namespace, (str, unicode)):
		namespace = sys.modules[namespace]

	if name is None:
		name = os.path.splitext(os.path.basename(path))[0]

	import imp
	module = imp.load_source(name, path)

	# Implements reloading of module
	module.__reload__ = lambda module: (import_file(namespace, module.__file__), module)[-1]
	_extend_builtin_reload()

	try:
		namespace.update(module.__dict__)
	except AttributeError:
		namespace.__dict__.update(module.__dict__)


def test_import_file():
	"""This test does not work if pytest is launched on a directory"""
	import tempfile
	with tempfile.NamedTemporaryFile() as src:
		module = type(b'Module', (object, ), {})()
		module.a = 1

		# Loads module with a custom file extension (without file extension)
		src.write('"Test module"\n\nb = 2\n')
		src.flush()
		src.seek(0)
		import_file(namespace=module, path=src.name)
		assert [(k, v) for k, v in sorted(module.__dict__.items()) if not k.startswith('_')] == \
			[('a', 1), ('b', 2)]

		# Checks if 'reload' works right
		src.write('"Test module"\n\nc = 3\n')
		src.seek(0)
		reload(module)
		assert [(k, v) for k, v in sorted(module.__dict__.items()) if not k.startswith('_')] == \
			[('a', 1), ('b', 2), ('c', 3)]


def read_file(path):
	"""Reads file, returns its content"""
	with open(path) as src:
		return src.read()


def print_arguments(prefix=None, returns=None):
	"""Prints out prefix (if provided) and function arguments

	Example:

		>>> print_arguments()('a', 1, b=2)
		('a', 1) {'b': 2}

		>>> print_arguments(prefix='Test:')(2, 5, c=3)
		Test: (2, 5) {'c': 3}

	"""
	def _print_arguments(*args, **kwargs):
		if prefix is not None:
			print prefix,
		print args, kwargs
		return returns
	return _print_arguments


def raise_stop_iteration():
	raise StopIteration()


def md5(value):
	"""Returns md5 from data

	Example:

		>>> md5('abc')
		'900150983cd24fb0d6963f7d28e17f72'

		>>> md5([1, 2, 3])
		'202cb962ac59075b964b07152d234b70'

		>>> md5(dict(a=1, b=2))
		'187ef4436122d1cc2f40dc2b92f0eba0'

	"""
	import md5

	if isinstance(value, (tuple, list, dict)):
		value = ''.join(unicode(x) for x in sorted(value))

	return md5.md5(value).hexdigest()


def json_serialize(value):
	"""Retuns a JSON-presentation of value

	Example:

		>>> json_serialize(dict(a=1, b=[2, 3, 4]))
		'{\\n    "a": 1, \\n    "b": [\\n        2, \\n        3, \\n        4\\n    ]\\n}'

	"""
	return json.dumps(value, indent=4)


def patch_wrapper_lineno(src, dst):
	"""Applies a patch to change a line number of wrapper to the line number of original function (for doc-tests)"""
	import types
	dst.func_code = types.CodeType(
		dst.func_code.co_argcount,
		dst.func_code.co_nlocals,
		dst.func_code.co_stacksize,
		dst.func_code.co_flags,
		dst.func_code.co_code,
		dst.func_code.co_consts,
		dst.func_code.co_names,
		dst.func_code.co_varnames,
		dst.func_code.co_filename,
		dst.func_code.co_name,
		src.func_code.co_firstlineno,
		dst.func_code.co_lnotab,
		dst.func_code.co_freevars,
		dst.func_code.co_cellvars,
	)


def contextmanager(src):
	"""Fixed context manager (for doc-tests)"""
	import contextlib
	dst = contextlib.contextmanager(src)
	patch_wrapper_lineno(src, dst)  # Patch for doc-tests
	return dst


def mock_modules(*module_names):
	"""Replaces some missing modules with mock-objects

	Example:

		>>> mock_modules('test_module', 'another_test_module')
		>>> import test_module, another_test_module
		>>> test_module, another_test_module  # doctest: +ELLIPSIS
		(<Mock name=u'module' id='...'>, <Mock name=u'module' id='...'>)

	"""
	for module_name in module_names:
		import mock
		sys.modules[module_name] = mock.Mock(name='module')


@contextmanager
def MockedStdout():
	class Stdout(list):
		def write(self, value):
			self.append(value)

		def __unicode__(self):
			return ''.join(unicode(x) for x in self)
	stdout = Stdout()

	with ReplacedObject(namespace='sys', name='stdout', replacement=stdout):
		yield stdout


@contextmanager
def MockedOpen(namespace='__builtin__', map=None):
	"""Context manager to temporarily replace Python built-in "open" with a mock-object

	Arguments:

		namespace -- module name where to patch (default: __builtin__). Use __main__ to patch current module.
		map -- data to present if file read method requested. Format: {'path regex pattern': 'data', ...}

		Yields a list object contained data written with a file write method.

	Example:

		>>> with MockedOpen():
		...		open  # doctest: +ELLIPSIS
		<function _open at 0x...>

		>>> with MockedOpen(map={
		...			'.*nonexistent.*': 'some data',
		...			'.*another.*': 'another data',
		...			'.*third.*': 'first\\nsecond\\nthird',
		...		}):
		...		with open('some/nonexistent/path') as src:
		...			src.name
		...			src.read()
		...		with open('some/another/path') as src:
		...			src.readlines()
		...		with open('some/third/path') as src:
		...			for line in src:
		...				line
		'some/nonexistent/path'
		'some data'
		['another data']
		'first\\n'
		'second\\n'
		'third'

		>>> with MockedOpen() as _dst:
		...		with open('some/nonexistent/path') as dst:
		...			dst.write('some data')
		...			_dst
		...		with open('another/nonexistent/path') as dst:
		...			dst.write('another data')
		...			_dst
		{'some/nonexistent/path': ['some data']}
		{'another/nonexistent/path': ['another data'], 'some/nonexistent/path': ['some data']}

	"""
	dst = {}

	def _open(path, *args, **kwargs):
		import mock
		f = mock.MagicMock(name='file_object', args=args, kwargs=kwargs)
		f.name = path
		f.__enter__ = lambda x: x
		f.write = dst.setdefault(path, []).append

		def _read():
			import re
			for pattern, value in (map or {}).items():
				pattern = re.compile(pattern)
				if pattern.match(f.name) is not None:
					return value
			else:
				# Wont raise if "try: ... except:" found in-between
				raise BaseException('No pattern found for path "{}"'.format(f.name))  # BaseSelection was selected in order to catch it only with test
		f.read = _read

		def _readlines():
			import re
			for pattern, value in (map or {}).items():
				pattern = re.compile(pattern)
				if pattern.match(f.name) is not None:
					return value.splitlines()
			else:
				# Wont raise if "try: ... except:" found in-between
				raise BaseException('No pattern found for path "{}"'.format(f.name))  # BaseSelection was selected in order to catch it only with test
		f.readlines = _readlines

		def readline_generator():
			for line in f.read().splitlines(True):
				yield line
		f.readline = readline_generator().next
		f.__iter__ = lambda x: iter(x.readline, '')  # Patch for adding __iter__ support into mock_open

		return f

	import mock
	with mock.patch('{}.open'.format(namespace), _open):
		yield dst


def run_MockedOpen():
	"""Entry point to check patching of open-function. Only for developing purposes."""
	with MockedOpen():
		print open


@contextmanager
def ReplacedObject(namespace, map=None, name=None, replacement=None):
	"""Temporarily replaces some object with its replacement.

	Example:

		>>> def _some_function():
		...		return 'some result'
		>>> _some_function()
		'some result'
		>>> with ReplacedObject(namespace=locals(), name='_some_function', replacement=lambda: 'another result'):
		...		_some_function()
		'another result'
		>>> _some_function()
		'some result'

		>>> def _some_function():
		...		return 'some result'
		>>> _some_function()
		'some result'
		>>> with ReplacedObject(namespace=locals(), map={
		...			'_some_function': lambda: 'another result',
		...		}):
		...			_some_function()
		'another result'
		>>> _some_function()
		'some result'

	"""
	previous = dict()

	if map is None:
		map = dict()
	if name is not None:
		map[name] = replacement

	if isinstance(namespace, (str, unicode)):
		namespace = sys.modules[namespace].__dict__

	# Does replacement
	for name, replacement in map.items():
		try:
			previous[name], namespace[name] = namespace[name], replacement
		except TypeError:
			previous[name] = getattr(namespace, name)
			setattr(namespace, name, replacement)

	try:
		yield
	finally:
		# Undoes replacement
		for name, replacement in map.items():
			try:
				namespace[name] = previous[name]
			except TypeError:
				setattr(namespace, name, previous[name])


def _subprocess_replacement(map, exit_code, command, *args, **kwargs):
	if isinstance(command, (tuple, list)):
		command = ' '.join(command)

	print 'Command:', command

	if map is not None:
		import re
		for pattern, exit_code in map.items():
			pattern = re.compile(pattern)
			if pattern.match(command) is not None:
				break
		else:
			# Wont raise if "try: ... except:" found in-between
			raise BaseException('No pattern found for command "{}"'.format(command))  # BaseSelection was selected in order to catch it only with test
	return exit_code() if callable(exit_code) else exit_code


@contextmanager
def MockedOsSystem(replacement=None, map=None, exit_code=0):
	"""Temporary replaces os.system with a mocked function

	Example:

		>>> import os

		>>> os.system  # doctest: +ELLIPSIS
		<built-in function system>
		>>> with MockedOsSystem():
		...		os.system  # doctest: +ELLIPSIS
		<function replacement at 0x...>
		>>> os.system  # doctest: +ELLIPSIS
		<built-in function system>

		>>> with MockedOsSystem():
		...		os.system('test command')
		Command: test command
		0

		>>> with MockedOsSystem(map={
		...			'succeed command': 0,
		...			'failed command': 1,
		...		}):
		...		os.system('succeed command')
		...		os.system('failed command')
		Command: succeed command
		0
		Command: failed command
		1

	"""
	if replacement is None:
		def replacement(command, *args, **kwargs):
			return _subprocess_replacement(map, exit_code, command, *args, **kwargs)

	with ReplacedObject(namespace=os, name='system', replacement=replacement):
		yield


@contextmanager
def MockedSubprocessPopen(replacement=None, map=None, pid=None, exit_code=0):
	"""Temporary replaces subprocess.Popen with a mocked function

	Example:

		>>> import subprocess

		>>> subprocess.Popen  # doctest: +ELLIPSIS
		<class 'subprocess.Popen'>
		>>> with MockedSubprocessPopen():
		...		subprocess.Popen  # doctest: +ELLIPSIS
		<function replacement at 0x...>
		>>> subprocess.Popen  # doctest: +ELLIPSIS
		<class 'subprocess.Popen'>

		>>> with MockedSubprocessPopen():
		...		process = subprocess.Popen('test command', shell=True)
		...		process.wait()
		Command: test command
		0

		>>> with MockedSubprocessPopen(map={
		...			'succeed command': 0,
		...			'failed command': 1,
		...		}):
		...		process = subprocess.Popen('succeed command', shell=True)
		...		process.wait()
		...		process = subprocess.Popen('failed command', shell=True)
		...		process.wait()
		Command: succeed command
		0
		Command: failed command
		1

	"""
	if replacement is None:
		def replacement(command, *args, **kwargs):
			import mock
			process = mock.Mock(name='subprocess')
			process.wait.return_value = _subprocess_replacement(map, exit_code, command, *args, **kwargs)
			process.pid = pid
			return process

	with ReplacedObject(namespace='subprocess', name='Popen', replacement=replacement):
		yield


@contextmanager
def MockedSubprocessCall(replacement=None, map=None, exit_code=0):
	"""Temporary replaces subprocess.call with a mocked function


	Example:

		>>> import subprocess

		>>> subprocess.call  # doctest: +ELLIPSIS
		<function call at 0x...>
		>>> with MockedSubprocessCall():
		...		subprocess.call  # doctest: +ELLIPSIS
		<function replacement at 0x...>
		>>> subprocess.call  # doctest: +ELLIPSIS
		<function call at 0x...>

		>>> with MockedSubprocessCall():
		...		subprocess.call('test command', shell=True)
		Command: test command
		0

		>>> with MockedSubprocessCall(exit_code=1):
		...		subprocess.call('test command', shell=True)
		Command: test command
		1

		>>> with MockedSubprocessCall(map={
		...			'succeed command': 0,
		...			'failed command': 1,
		...		}):
		...		subprocess.call('succeed command', shell=True)
		...		subprocess.call('failed command', shell=True)
		Command: succeed command
		0
		Command: failed command
		1

	"""
	if replacement is None:
		def replacement(command, *args, **kwargs):
			return _subprocess_replacement(map, exit_code, command, *args, **kwargs)

	with ReplacedObject(namespace='subprocess', name='call', replacement=replacement):
		yield


@contextmanager
def MockedSubprocessCheckOutput(replacement=None, map=None, result=None):
	"""Temporary replaces subprocess.check_output with a mocked function

	Example:

		>>> import subprocess

		>>> subprocess.check_output  # doctest: +ELLIPSIS
		<function check_output at 0x...>
		>>> with MockedSubprocessCheckOutput():
		...		subprocess.check_output  # doctest: +ELLIPSIS
		<function replacement at 0x...>
		>>> subprocess.check_output  # doctest: +ELLIPSIS
		<function check_output at 0x...>

		>>> with MockedSubprocessCheckOutput(result='Some result'):
		...		subprocess.check_output('test command', shell=True)
		Command: test command
		'Some result'

		>>> with MockedSubprocessCheckOutput(map={
		...			'Some command': 'Some result',
		...			'test.*': 'test result',
		...		}):
		...		subprocess.check_output('test command', shell=True)
		Command: test command
		'test result'

	"""
	if replacement is None:
		def replacement(command, *args, **kwargs):
			return _subprocess_replacement(map, result, command, *args, **kwargs)

	with ReplacedObject(namespace='subprocess', name='check_output', replacement=replacement):
		yield


# @contextmanager
# def PatchedLDAP(data=None):
#     """Patches ldap-module in order to log communication with ldap-server or to simulate it

#     Example:

#         >>> import ldap

#         >>> with PatchedLDAP():
#         ...		pass

#         >>> with PatchedLDAP(data=[]):
#         ...		pass

#     """
#     cache = []

#     import ldap.functions

#     def decorator(function):
#         def wrapper(lock, func, *args, **kwargs):
#             original = function(lock, func, *args, **kwargs)

#             def decorator(name, function):
#                 def wrapper(*args, **kwargs):
#                     key = json.dumps((name, args, kwargs))

#                     if data is not None:
#                         for k, v in data:
#                             if k == key:
#                                 data.remove((k, v))
#                                 result = json.loads(v)
#                                 break
#                         else:
#                             raise BaseException('Key "{}" not found'.format(key))
#                     else:
#                         # logging.getLogger(__name__).warning('%s', (name, args, kwargs))
#                         result = function(*args, **kwargs)
#                         # logging.getLogger(__name__).warning('%s', result)

#                         cache.append((key, json.dumps(result)))
#                     return result
#                 return wrapper

#             class Wrapper(object):
#                 def __getattr__(self, key):
#                     return decorator(key, getattr(original, key))

#                 def __setattr__(self, key, value):
#                     # logging.getLogger(__name__).warning('(key, value)=' + '%s', (key, value))
#                     return setattr(original, key, value)

#                 def __delattr__(self, key):
#                     # logging.getLogger(__name__).warning('key=' + '%s', key)
#                     return delattr(original, key)

#             return Wrapper()
#         return wrapper
#     previous_function, ldap.functions._ldap_function_call = ldap.functions._ldap_function_call, decorator(ldap.functions._ldap_function_call)

#     try:
#         yield cache
#     finally:
#         ldap.functions._ldap_function_call = previous_function


@contextmanager
def PatchedUrllib3PoolManager(data=None):
	"""Patches urllib3-module in order to log communication with http-server or to simulate it

	Example:

		>>> import urllib3

		Comment it out in order to generate data for simulation:

		# >>> with PatchedUrllib3PoolManager() as data:
		# ...		pool = urllib3.PoolManager()
		# ...		pool.request('GET', 'http://ya.ru')
		# ...		data  # doctest: +ELLIPSIS

		>>> data = [('["request", ["GET", "http://ya.ru"], {}]', '[200, "<!DOCTYPE html><html><title></title></head><body>Some content</body></html>"]')]
		>>> with PatchedUrllib3PoolManager(data):
		...		pool = urllib3.PoolManager()
		...		result = pool.request('GET', 'http://ya.ru')  # doctest: +ELLIPSIS
		...		result.status, result.data
		(200, u'<!DOCTYPE html><html><title></title></head><body>Some content</body></html>')

	"""
	cache = []

	import urllib3

	def decorator(function):
		def wrapper(*args, **kwargs):
			original = function(*args, **kwargs)

			def decorator(name, function):
				def wrapper(*args, **kwargs):
					key = json.dumps((name, args, kwargs))

					if data is not None:
						for k, v in data:
							if k == key:
								data.remove((k, v))
								result = type(b'Result', (object, ), {})()
								result.status, result.data = json.loads(v)
								break
						else:
							raise BaseException('Key "{}" not found'.format(key))
					else:
						# logging.getLogger(__name__).warning('%s', (name, args, kwargs))
						result = function(*args, **kwargs)
						# logging.getLogger(__name__).warning('%s', result)

						cache.append((key, json.dumps((result.status, result.data))))
					return result
				return wrapper

			class Wrapper(object):
				def __getattr__(self, key):
					return decorator(key, getattr(original, key))

				def __setattr__(self, key, value):
					# logging.getLogger(__name__).warning('(key, value)=' + '%s', (key, value))
					return setattr(original, key, value)

				def __delattr__(self, key):
					# logging.getLogger(__name__).warning('key=' + '%s', key)
					return delattr(original, key)

			return Wrapper()
		return wrapper
	previous_function, urllib3.PoolManager = urllib3.PoolManager, decorator(urllib3.PoolManager)

	try:
		yield cache
	finally:
		urllib3.PoolManager = previous_function


@contextmanager
def ImportPath(path):
	"""Temporary inserts path into sys.path

	Example:

		>>> sys.path.insert(0, '..')
		>>> sys.path[0]
		'..'
		>>> with ImportPath('.'):
		...		sys.path[0]
		'.'
		>>> sys.path.pop(0)
		'..'

	"""
	sys.path.insert(0, path)
	try:
		yield
	finally:
		sys.path.pop(0)


def get_tests(path, prefix=None):
	"""Reads files found in path, generates a list with data to test

	Doc-Tests
	=========

		>>> def test_get_tests():
		...		with \\
		...			ReplacedObject(namespace='glob', name='glob', replacement=lambda x: (
		...				['tests/test_{}'.format(x) for x in ['a', 'b', 'cp', 'd']]
		...			)), \\
		...			MockedOpen(map={
		...				'tests/test_a': "{'src': 1, 'dst': 'Path: tests/test_a\\\\n1'}",
		...				'tests/test_b': "{'src': None, 'dst': 'Path: tests/test_b\\\\n'}",
		...				'tests/test_cp': "{'src': None, 'dst': None}",
		...				'tests/test_d': "{'src': 1, 'dst': 2}",
		...			}):
		...				for test in get_tests(path='tests/test_*'):
		...					if not test.path.endswith('p'):
		...						print 'Path:', test.path
		...					test.src == test.dst

		>>> test_get_tests()  # doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
		Traceback (most recent call last):
			File "/usr/lib/python2.7/doctest.py", line 1315, in __run
				compileflags, 1) in test.globs
			File "<doctest helpers.tests.get_tests[1]>", line 1, in <module>
				test_get_tests()  # doctest: +NORMALIZE_WHITESPACE
			File "<doctest helpers.tests.get_tests[0]>", line 15, in test_get_tests
				test.src == test.dst
			File "...", line 803, in __eq__
				raise ValueError(...)
		ValueError: expected and got values differ:
		---
		+++
		@@ -1,2 +1 @@
		-Path: tests/test_a
		1

	"""
	try:
		for _path in glob.glob(path):
			serialized_data = read_file(_path)

			# Parses serialized data
			data = ast.literal_eval(serialized_data)

			# Creates wrappers in order to simplify comparison between result and dst-value
			class Test(object):
				def __init__(self, **kwargs):
					# Customizes __eq__-method for dst
					class ComparatorMixture(dict):
						def __eq__(self, result):
							awaited, got = textwrap.dedent(unicode(self['value'])).lstrip(), unicode(result)
							if awaited != got:
								awaited, got = awaited.splitlines(True), got.splitlines(True)
								# Calculates difference
								import difflib
								diff = difflib.unified_diff(awaited, got, n=2)  # UDIFF
								# diff = difflib.context_diff(awaited, got, n=2)  # CDIFF
								# diff = difflib.Differ(charjunk=difflib.IS_CHARACTER_JUNK).compare(awaited, got)  # NDIFF
								raise ValueError('expected and got values differ: \n{}'.format(''.join(diff)))
					kwargs['dst'] = ComparatorMixture(value=kwargs['dst'])

					self.__dict__.update(kwargs)
			yield Test(path=_path, src=data['src'], dst=data['dst'])
	finally:
		pass


def _main():
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, add_help=False)
	parser.add_argument('-r', '--run-function', required=True, choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	_main()
