#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:

"""
Module to provide various modifications of cache

Doc-Tests
=========

Mock-patching of environment:

	>>> import logging, os, sys
	>>> sys.path.insert(0, os.path.dirname(__file__) or '.')

"""

from __future__ import division, unicode_literals; del division, unicode_literals  # Imports features but removes them from documentation
import json
import logging
import os
import sys
import time

if __name__ == '__main__':
	# Sets utf-8 (instead of latin1) as default encoding for every IO
	reload(sys); sys.setdefaultencoding('utf-8')
	# Configures logging
	logging.basicConfig(level=logging.WARNING, datefmt='%H:%M:%S', format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s')


class _AbstractDecorator(object):
	"""Decorates function.

	Doc-Tests
	=========

		>>> class SomeDecorator(_AbstractDecorator):
		...		def _called(self):
		...			return 'before+' + self._function() + '+after'

		>>> def function1():
		...		return 'something1'
		>>> cached_function = SomeDecorator(function1)
		>>> cached_function()
		'before+something1+after'

		>>> def function2():
		...		return 'something2'
		>>> cached_function = SomeDecorator()(function2)
		>>> cached_function()
		'before+something2+after'

		>>> @SomeDecorator()
		... def function3():
		...		return 'something3'
		>>> function3()
		'before+something3+after'

	"""
	def __new__(cls, *args, **kwargs):
		self = super(_AbstractDecorator, cls).__new__(cls)
		self.__init__(*args, **kwargs)
		if self._function is not None:
			self = self._wrap(self)
		return self

	def __init__(self, function=None):
		# Decorates function (from first argument)
		self._function = function

	@staticmethod
	def _wrap(self):
		"""Returns a function instead of class instance (fix for doc-test).

		Replaces some attributes with the ones from original function
		"""
		def _wrapper(*args, **kwargs):
			return self(*args, **kwargs)
		_wrapper.__module__ = self._function.__module__
		_wrapper.__name__ = self._function.__name__
		_wrapper.__doc__ = self._function.__doc__

		# Modifies wrapper with a first line number from the original function
		import tests
		tests.patch_wrapper_lineno(self._function, _wrapper)  # Patch for doc-tests

		return _wrapper

	def _called(self, *args, **kwargs):
		raise NotImplementedError('Virtual method')

	def __call__(self, *args, **kwargs):
		if self._function is None:
			# Decorates function (from first argument)
			self._function = kwargs.get('function', args[0])
			return self._wrap(self)
			# return self
		else:
			return self._called(*args, **kwargs)


class _AbstractFilesystemCache(_AbstractDecorator):
	def __init__(self, *args, **kwargs):
		self._path = kwargs.pop('path')
		super(_AbstractFilesystemCache, self).__init__(*args, **kwargs)

	def _get_path(self, *args, **kwargs):
		return '{self._path}'.format(**locals()) + '{args}_{kwargs}'.format(**locals()).replace('/', '%2f').replace('*', '%2a')

	def _save(self, path, value):
		with open(path, 'w') as dst:
			dst.write(value)

	def _restore(self, path):
		try:
			with open(path) as src:
				value = src.read()
		except Exception:
			value = None
		return value


class IgnoreExceptionsFilesystemCache(_AbstractFilesystemCache):
	"""Loads data from cache only if some exceptions occur.

	Doc-Tests
	=========

		>>> import tests

		>>> with \\
		...		tests.MockedOpen(namespace=__name__, map={'./.cache_.*': '"anything"'}) as dst, \\
		...		tests.ReplacedObject(namespace=os, name='utime', replacement=lambda *args: None):
		...			def function():
		...				return 'something'
		...			cached_function = IgnoreExceptionsFilesystemCache(function, exceptions=(KeyError, ValueError), path='./.cache_')
		...			cached_function(), dst  # Value and content of file-cache
		('something', {u'./.cache_()_{}': ['"something"']})

		>>> with \\
		...		tests.MockedOpen(namespace=__name__, map={'./.cache_.*': '"anything"'}) as dst, \\
		...		tests.ReplacedObject(namespace=os, name='utime', replacement=lambda *args: None):
		...			def function():
		...				return 'something'
		...			cached_function = IgnoreExceptionsFilesystemCache(exceptions=(KeyError, ValueError), path='./.cache_')(function)
		...			cached_function(), dst  # Value and content of file-cache
		('something', {u'./.cache_()_{}': ['"something"']})

		# Funktion returns value, cache saves it in a file-cache
		>>> with \\
		...		tests.MockedOpen(namespace=__name__, map={'./.cache_.*': '"anything"'}) as dst, \\
		...		tests.ReplacedObject(namespace=os, name='utime', replacement=lambda *args: None):
		...			@IgnoreExceptionsFilesystemCache(exceptions=(KeyError, ValueError), path='./.cache_')
		...			def function():
		...				return 'something'
		...			function(), dst  # Value and content of file-cache
		('something', {u'./.cache_()_{}': ['"something"']})

		# Function raises exception, but the cached value is returned
		>>> with \\
		...		tests.MockedOpen(namespace=__name__, map={'./.cache_.*': '"anything"'}) as dst, \\
		...		tests.ReplacedObject(namespace=os, name='utime', replacement=lambda *args: None), \\
		...		tests.ReplacedObject(namespace=logging.getLogger(__name__), name='warning', replacement=tests.print_arguments(prefix='Warning:')):
		...			@IgnoreExceptionsFilesystemCache(exceptions=(KeyError, ValueError), path='./.cache_')
		...			def function():
		...				raise KeyError()
		...			function()  # Returns cached value (if exception occurs)
		...			# doctest: +ELLIPSIS
		Warning: (u'Exception during calling "%s": %s. Loading data from cache "%s"', <function function at 0x...>, KeyError(), u'./.cache_()_{}') {}
		u'anything'

	"""
	def __init__(self, *args, **kwargs):
		self._exceptions = kwargs.pop('exceptions')
		self._raise_if_no_cache = kwargs.get('raise_if_no_cache', False)
		super(IgnoreExceptionsFilesystemCache, self).__init__(*args, **kwargs)

	def _called(self, *args, **kwargs):
		path = self._get_path(*args, **kwargs)
		try:
			value = self._function(*args, **kwargs)
		except self._exceptions as e:
			logging.getLogger(__name__).warning('Exception during calling "%s": %s. Loading data from cache "%s"', self._function, e, path)
			serialized_value = self._restore(path)
			if serialized_value is None:
				value = None
				if self._raise_if_no_cache:
					raise
			else:
				value = json.loads(serialized_value)
		except Exception as e:
			raise
		else:
			serialized_value = json.dumps(value, indent=4)
			if serialized_value != self._restore(path):
				self._save(path, serialized_value)
		return value


def run_ignore_exceptions_filesystem_cache():
	"""Entry point to check cache. Only for developing purposes."""
	@IgnoreExceptionsFilesystemCache(exceptions=(KeyError, ValueError), path='./.cache_')
	def function():
		raise KeyError()
		return dict(a=1)
	result = function()
	logging.getLogger(__name__).warning('Result: (%s)\n%s', result.__class__.__name__, json.dumps(result, indent=4))


class ExpiredFilesystemCache(_AbstractFilesystemCache):
	"""Loads data from cache only if not expired.

	Doc-Tests
	=========

		>>> import tests

		>>> with \\
		...		tests.MockedOpen(namespace=__name__, map={'./.cache_.*': '"anything"'}) as dst, \\
		...		tests.ReplacedObject(namespace=os, name='utime', replacement=lambda *args: None):
		...			def function():
		...				return 'something'
		...			cached_function = ExpiredFilesystemCache(function, timeout=10, path='./.cache_')
		...			cached_function(), dst  # Value and content of file-cache
		('something', {u'./.cache_()_{}': ['"something"']})

		>>> with \\
		...		tests.MockedOpen(namespace=__name__, map={'./.cache_.*': '"anything"'}) as dst, \\
		...		tests.ReplacedObject(namespace=os, name='utime', replacement=lambda *args: None):
		...			def function():
		...				return 'something'
		...			cached_function = ExpiredFilesystemCache(timeout=10, path='./.cache_')(function)
		...			cached_function(), dst  # Value and content of file-cache
		('something', {u'./.cache_()_{}': ['"something"']})

		>>> with \\
		...		tests.MockedOpen(namespace=__name__, map={'./.cache_.*': '"anything"'}) as dst, \\
		...		tests.ReplacedObject(namespace=os, name='utime', replacement=lambda *args: None):
		...			@ExpiredFilesystemCache(timeout=10, path='./.cache_')
		...			def function():
		...				return 'something'
		...			function(), dst  # Value and content of file-cache
		('something', {u'./.cache_()_{}': ['"something"']})

	"""
	def __init__(self, *args, **kwargs):
		self._timeout = kwargs.pop('timeout')
		super(ExpiredFilesystemCache, self).__init__(*args, **kwargs)

	def _save(self, path, value):
		super(ExpiredFilesystemCache, self)._save(path, value)
		os.utime(path, (time.time(), time.time() + self._timeout))

	def _called(self, *args, **kwargs):
		path = self._get_path(*args, **kwargs)
		if os.path.exists(path) and time.time() < os.path.getmtime(path):
			logging.getLogger(__name__).info('Loading data from cache "%s"', path)
			serialized_value = self._restore(path)
			value = json.loads(serialized_value)
		else:
			value = self._function(*args, **kwargs)
			serialized_value = json.dumps(value, indent=4)
			if serialized_value == self._restore(path):
				os.utime(path, None)  # Only update modification and access times
			else:
				self._save(path, serialized_value)
		return value


def run_expired_filesystem_cache():
	"""Entry point to check cache. Only for developing purposes."""
	@ExpiredFilesystemCache(timeout=5, path='./.cache_')
	def function():
		return dict(a=time.time())
	result = function()
	logging.getLogger(__name__).warning('Result: (%s)\n%s', result.__class__.__name__, json.dumps(result, indent=4))


class ExpiredMemoryCache(_AbstractDecorator):
	"""Loads data from memory cache only if not expired.

	Doc-Tests
	=========

		>>> import tests

		>>> def function():
		...		return 'something'
		>>> cached_function = ExpiredMemoryCache(function, timeout=10)
		>>> cached_function()
		'something'

		or:

		>>> def function():
		...		return 'something'
		>>> cached_function = ExpiredMemoryCache(timeout=10)(function)
		>>> cached_function()
		'something'

		or:

		>>> @ExpiredMemoryCache(timeout=10)
		... def function():
		...		return 'something'
		>>> cached_function()
		'something'

	"""
	def __init__(self, *args, **kwargs):
		self._timeout = kwargs.pop('timeout')
		super(ExpiredMemoryCache, self).__init__(*args, **kwargs)
		self._cache = dict()

	def _called(self, *args, **kwargs):
		key = '{args}_{kwargs}'.format(**locals())
		try:
			(value, expired) = self._cache[key]
			if time.time() >= expired:
				raise ValueError()
		except (KeyError, ValueError):
			value = self._function(*args, **kwargs)
			self._cache[key] = (value, time.time() + self._timeout)
		return value


def run_expired_memory_cache():
	"""Entry point to check cache. Only for developing purposes."""
	@ExpiredMemoryCache(timeout=2)
	def function():
		return dict(a=time.time())
	for index in range(10):
		result = function()
		logging.getLogger(__name__).warning('Result: (%s)\n%s', result.__class__.__name__, json.dumps(result, indent=4))
		time.sleep(.5)


def _main():
	import argparse
	parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, add_help=False)
	parser.add_argument('-r', '--run-function', required=True, choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
	kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

	globals()['run_' + kwargs['run_function']]()

if __name__ == '__main__':
	_main()
